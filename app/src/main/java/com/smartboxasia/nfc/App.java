package com.smartboxasia.nfc;

import android.app.Activity;
import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;

public class App extends Application {
    private static SQLiteDatabase DB;
    private static ArrayList<Activity> activities;
    private static boolean isOrienationActivated;
    private static NfcTag lastScannedTag;

    static {
        activities = new ArrayList();
    }

    public static SQLiteDatabase getDB() {
        return DB;
    }

    public static void setDB(SQLiteDatabase dB) {
        DB = dB;
    }

    public static NfcTag getLastScannedTag() {
        return lastScannedTag;
    }

    public static void setLastScannedTag(NfcTag lastScannedTag) {
        lastScannedTag = lastScannedTag;
    }

    public static ArrayList<Activity> getActivities() {
        return activities;
    }

    public static void setActivities(ArrayList<Activity> activities) {
        activities = activities;
    }

    public static boolean isOrienationActivated() {
        return isOrienationActivated;
    }

    public static void setOrienationActivated(boolean isOrienationActivated) {
        isOrienationActivated = isOrienationActivated;
    }
}
