package com.smartboxasia.nfc;

public class Daytime {
    private int beginHour;
    private int beginMin;
    private int daytimeId;
    private int endHour;
    private int endMin;
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBeginHour() {
        return this.beginHour;
    }

    public void setBeginHour(int beginHour) {
        this.beginHour = beginHour;
    }

    public int getBeginMin() {
        return this.beginMin;
    }

    public void setBeginMin(int beginMin) {
        this.beginMin = beginMin;
    }

    public int getEndHour() {
        return this.endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    public int getEndMin() {
        return this.endMin;
    }

    public void setEndMin(int endMin) {
        this.endMin = endMin;
    }

    public int getDaytimeId() {
        return this.daytimeId;
    }

    public void setDaytimeId(int daytimeId) {
        this.daytimeId = daytimeId;
    }
}
