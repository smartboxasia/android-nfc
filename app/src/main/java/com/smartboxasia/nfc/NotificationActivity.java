package com.smartboxasia.nfc;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.nfc.Tag;
import android.os.Bundle;
import android.provider.Settings.System;
import android.support.v4.app.NotificationCompat.Builder;
import java.util.Iterator;

public class NotificationActivity extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        App.getActivities().add(this);
        App.setLastScannedTag(null);
        App.setDB(new DbOps(this).getWritableDatabase());
        App.setOrienationActivated(getSystemSettingRotation());
        Tag detectedTag = (Tag) getIntent().getParcelableExtra("android.nfc.extra.TAG");
        String actualNfcHexId = changeIdtoHexString(detectedTag.getId());
        if (checkNfcTagExisting(changeIdtoHexString(detectedTag.getId()))) {
            Cursor cFoundTag = App.getDB().rawQuery("SELECT * FROM nfctag WHERE nfctag_hexID='" + actualNfcHexId + "'", null);
            cFoundTag.moveToFirst();
            NfcTag lastScannedTag = new NfcTag();
            lastScannedTag.setNfctagId(cFoundTag.getInt(0));
            lastScannedTag.setNfctagHexId(cFoundTag.getString(1));
            lastScannedTag.setNfctagName(cFoundTag.getString(2));
            lastScannedTag.setNfctagHint(cFoundTag.getString(3));
            lastScannedTag.setRuleDaytime(cFoundTag.getInt(4));
            App.setLastScannedTag(lastScannedTag);
            String tagString = "";
            if (cFoundTag.getString(3).equals("")) {
                tagString = cFoundTag.getString(2);
            } else {
                tagString = cFoundTag.getString(2) + " (" + cFoundTag.getString(3) + ")";
            }
            Builder mBuilder = new Builder(this);
            mBuilder.setAutoCancel(true);
            NotificationManager mNotificationManager = (NotificationManager) getSystemService("notification");
            Intent enterApp = new Intent(this, MainActivity.class);
            enterApp.setAction("ch.zingge.ds_nfc.action.ENTER_WITH_KNOWN_TAG");
            enterApp.putExtra("nfctagId", lastScannedTag.getNfctagId());
            mBuilder.setContentIntent(PendingIntent.getActivity(this, 0, enterApp, 0));
            if (CheckOps.getActionIdToCall(lastScannedTag, getScreenOrientation(), this).length() <= 4) {
                Cursor cSceneString = App.getDB().rawQuery("SELECT scenename FROM actions WHERE actionID=" + CheckOps.getActionIdToCall(lastScannedTag, getScreenOrientation(), this), null);
                cSceneString.moveToFirst();
                String sceneString = cSceneString.getString(0);
                callIntent(CheckOps.getActionIdToCall(lastScannedTag, getScreenOrientation(), this));
                mBuilder.setSmallIcon(R.drawable.ic_launcher_small);
                mBuilder.setContentTitle(tagString);
                mBuilder.setContentText(sceneString);
                if (getSystemSettingRotation()) {
                    if (getScreenOrientation() == 0) {
                        mBuilder.setContentInfo(getString(R.string.notification_toast_portrait));
                    } else {
                        mBuilder.setContentInfo(getString(R.string.notification_toast_landscape));
                    }
                }
                mNotificationManager.notify(1, mBuilder.build());
            } else {
                mBuilder.setSmallIcon(R.drawable.ic_launcher_small);
                mBuilder.setContentTitle(tagString);
                mBuilder.setContentText(CheckOps.getActionIdToCall(lastScannedTag, getScreenOrientation(), this));
                if (getSystemSettingRotation()) {
                    if (getScreenOrientation() == 0) {
                        mBuilder.setContentInfo(getString(R.string.notification_toast_portrait));
                    } else {
                        mBuilder.setContentInfo(getString(R.string.notification_toast_landscape));
                    }
                }
                mNotificationManager.notify(1, mBuilder.build());
            }
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setAction("ch.zingge.ds_nfc.action.ENTER_WITH_UNKNOWN_TAG");
            String str = "hexId";
            intent.putExtra(str, changeIdtoHexString(detectedTag.getId()));
            startActivity(intent);
        }
        Iterator it = App.getActivities().iterator();
        while (it.hasNext()) {
            ((Activity) it.next()).finish();
        }
    }

    public boolean getSystemSettingRotation() {
        if (System.getInt(getContentResolver(), "accelerometer_rotation", 0) == 1) {
            return true;
        }
        return false;
    }

    public int getScreenOrientation() {
        int orientation = getWindowManager().getDefaultDisplay().getRotation();
        if (orientation != 0) {
            orientation = 1;
        }
        // System.out.println(orientation);
        return orientation;
    }

    public String changeIdtoHexString(byte[] id) {
        String hexId = "";
        for (byte idPart : id) {
            if (Integer.toHexString(idPart).length() > 2) {
                hexId = new StringBuilder(String.valueOf(Integer.toHexString(idPart).substring(6, 8))).append(hexId).toString();
            } else {
                hexId = Integer.toHexString(idPart) + hexId;
            }
        }
        return hexId;
    }

    public boolean checkNfcTagExisting(String tagId) {
        App.getDB().setForeignKeyConstraintsEnabled(true);
        Cursor nfcTags = App.getDB().rawQuery("SELECT nfctag_hexID FROM nfctag", null);
        while (nfcTags.moveToNext()) {
            if (tagId.equals(nfcTags.getString(0))) {
                return true;
            }
        }
        return false;
    }

    public void callIntent(String id) {
        Intent serviceIntent = new Intent("com.smartboxasia.control.action.CALL");
        serviceIntent.putExtra("com.smartboxasia.control.callingPackage", getPackageName());
        serviceIntent.putExtra("com.smartboxasia.control.actionIdentifier", id);
        if (getPackageManager().queryIntentServices(serviceIntent, 0).size() > 0) {
            startService(serviceIntent);
        }
    }
}
