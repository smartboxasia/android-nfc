package com.smartboxasia.nfc;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

@SuppressLint({"SimpleDateFormat"})
public class CheckOps {
    public static int getActualDaytimeId() {
        int stunde = Integer.parseInt(new SimpleDateFormat("H").format(Calendar.getInstance().getTime()));
        int zeit = (stunde * 100) + Integer.parseInt(new SimpleDateFormat("m").format(Calendar.getInstance().getTime()));
        int[] row;
        Cursor cDaytime = App.getDB().rawQuery("SELECT daytimeID, begin_hour, begin_minute, end_hour, end_minute FROM daytime", null);
        ArrayList<int[]> sammlung = new ArrayList();
        while (cDaytime.moveToNext()) {
            row = new int[4];
            row[0] = cDaytime.getInt(0);
            row[1] = (cDaytime.getInt(1) * 100) + cDaytime.getInt(2);
            row[2] = (cDaytime.getInt(3) * 100) + cDaytime.getInt(4);
            if (row[1] < row[2]) {
                row[3] = 0;
            } else if (row[1] == row[2]) {
                row[3] = 2;
            } else {
                row[3] = 1;
            }
            sammlung.add(row);
        }
        int daytimeResult = 0;
        Iterator it = sammlung.iterator();
        while (it.hasNext()) {
            row = (int[]) it.next();
            if (row[1] <= zeit && row[2] >= zeit) {
                daytimeResult = row[0];
            }
        }
        if (daytimeResult == 0) {
            it = sammlung.iterator();
            while (it.hasNext()) {
                row = (int[]) it.next();
                if (row[3] == 1 && ((row[1] <= zeit && 2359 >= zeit) || (zeit >= 0 && row[2] >= zeit))) {
                    daytimeResult = row[0];
                }
            }
        }
        return daytimeResult;
    }

    public static String getNewActionId() {
        Cursor cMaxActionId = App.getDB().rawQuery("SELECT MAX(actionID) FROM actions", null);
        cMaxActionId.moveToFirst();
        if (cMaxActionId.isNull(0)) {
            return "1";
        }
        return String.valueOf(Integer.parseInt(cMaxActionId.getString(0)) + 1);
    }

    public static String getActualActionId() {
        Cursor cMaxActionId = App.getDB().rawQuery("SELECT MAX(actionID) FROM actions", null);
        cMaxActionId.moveToFirst();
        if (cMaxActionId.isNull(0)) {
            return "0";
        }
        return cMaxActionId.getString(0);
    }

    public static String getActionIdToCall(NfcTag scannedNfcTag, int orientation, Context context) {
        String actionIdToCall = context.getString(R.string.notification_toast_keine_passende_szene);
        if (scannedNfcTag.getRuleDaytime() == 1) {
            Cursor cActionIdToCallDaytimeOn = App.getDB().rawQuery("SELECT actionID FROM actions WHERE FK_nfctag=" + scannedNfcTag.getNfctagId() + " AND " + "FK_daytime=" + getActualDaytimeId() + " AND " + "orientation=" + orientation, null);
            if (cActionIdToCallDaytimeOn.moveToLast()) {
                actionIdToCall = cActionIdToCallDaytimeOn.getString(0);
            }
        }
        if (scannedNfcTag.getRuleDaytime() != 0) {
            return actionIdToCall;
        }
        Cursor cActionIdToCallDaytimeOff = App.getDB().rawQuery("SELECT actionID FROM actions WHERE FK_nfctag=" + scannedNfcTag.getNfctagId() + " AND " + "FK_daytime=1 AND " + "orientation=" + orientation, null);
        if (cActionIdToCallDaytimeOff.moveToLast()) {
            return cActionIdToCallDaytimeOff.getString(0);
        }
        return actionIdToCall;
    }
}
