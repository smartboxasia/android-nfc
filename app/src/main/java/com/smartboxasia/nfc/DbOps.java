package com.smartboxasia.nfc;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.Date;

public class DbOps extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "DB_dsNFC";
    public static final int DATABASE_VERSION = 1;

    public DbOps(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS settings (settingID INTEGER NOT NULL, settingname TEXT, settingvalue INTEGER, PRIMARY KEY (settingID ASC))");
        db.execSQL("CREATE TABLE IF NOT EXISTS daytime (daytimeID INTEGER NOT NULL, daytimename TEXT, begin_hour INT, begin_minute INT, end_hour INT, end_minute INT, PRIMARY KEY (daytimeID ASC))");
        db.execSQL("CREATE TABLE IF NOT EXISTS nfctag (nfctagID INTEGER NOT NULL, nfctag_hexID TEXT UNIQUE, nfctagname TEXT, nfctaghint TEXT, rule_daytime BOOLEAN, PRIMARY KEY (nfctagID))");
        db.execSQL("CREATE TABLE IF NOT EXISTS actions (actionID INTEGER NOT NULL, FK_daytime INTEGER, FK_nfctag INTEGER, timestamp DATETIME, scenename TEXT, orientation INTEGER, PRIMARY KEY (actionID ASC), FOREIGN KEY(FK_daytime) REFERENCES daytime(daytimeID), FOREIGN KEY(FK_nfctag) REFERENCES nfctag(nfctagID))");
        db.execSQL("CREATE TABLE IF NOT EXISTS log (logID INTEGER NOT NULL, timestamp DATETIME, actiontype TEXT, actionstring TEXT, PRIMARY KEY (logID ASC))");
        db.execSQL("INSERT INTO daytime (daytimename, begin_hour, begin_minute, end_hour, end_minute) VALUES ('Dummy', 0, 0, 0, 0)");
    }

    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
    }

    public static void insertAction(SQLiteDatabase db, int nfctagID, String sceneName) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("INSERT INTO actions (FK_daytime, FK_nfctag, timestamp, scenename) VALUES (" + CheckOps.getActualDaytimeId() + ", " + nfctagID + ", '" + new Date() + "', '" + sceneName + "')");
    }

    public static void insertAction(SQLiteDatabase db, int daytime, int nfctagID, String sceneName, int orientation) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("INSERT INTO actions (FK_daytime, FK_nfctag, timestamp, scenename, orientation) VALUES (" + daytime + ", " + nfctagID + ", '" + new Date() + "', '" + sceneName + "', " + orientation + ")");
    }

    public static void deleteAction(SQLiteDatabase db, Action action) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("DELETE FROM actions WHERE actionID=" + action.getActionId());
    }

    public static void deleteAction(SQLiteDatabase db, int nfctagId) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("DELETE FROM actions WHERE FK_nfctag=" + nfctagId);
    }

    public static void insertNfctag(SQLiteDatabase db, String nfctag_hexID, String tagName, String tagHint, int ruleDaytime) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("INSERT INTO nfctag (nfctag_hexID, nfctagname, nfctaghint, rule_daytime) VALUES ('" + nfctag_hexID + "', '" + tagName + "', '" + tagHint + "', " + ruleDaytime + ")");
    }

    public static void deleteNfctag(SQLiteDatabase db, String nfcTagHexID) {
        Cursor c = db.rawQuery("SELECT nfctagID FROM nfctag WHERE nfctag_hexID='" + nfcTagHexID + "'", null);
        c.moveToFirst();
        if (c.isNull(0)) {
            db.setForeignKeyConstraintsEnabled(true);
            db.execSQL("DELETE FROM nfctag WHERE nfctag_hexID = '" + nfcTagHexID + "'");
            return;
        }
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("DELETE FROM actions WHERE FK_nfctag=" + c.getString(0));
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("DELETE FROM nfctag WHERE nfctag_hexID = '" + nfcTagHexID + "'");
    }

    public static void updateNfctag(SQLiteDatabase db, String nfcTagHexID, int ruleDaytime) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("UPDATE nfctag SET rule_daytime=" + ruleDaytime + " WHERE nfctag_hexID='" + nfcTagHexID + "'");
    }

    public static void updateNfctag(SQLiteDatabase db, String nfcTagHexID, String nfctagName, String nfctagHint) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("UPDATE nfctag SET nfctagname='" + nfctagName + "', " + "nfctaghint='" + nfctagHint + "' WHERE " + "nfctag_hexID='" + nfcTagHexID + "'");
    }

    public static void insertDaytime(SQLiteDatabase db, String daytimeName, int beginHour, int beginMinute, int endHour, int endMinute) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("INSERT INTO daytime (daytimename, begin_hour, begin_minute, end_hour, end_minute) VALUES ('" + daytimeName + "', " + beginHour + ", " + beginMinute + ", " + endHour + ", " + endMinute + ")");
    }

    public static void deleteDaytime(SQLiteDatabase db, int daytimeId) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("DELETE FROM actions WHERE FK_daytime=" + daytimeId);
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("DELETE FROM daytime WHERE daytimeID=" + daytimeId);
    }

    public static void updateDaytimeBegin(SQLiteDatabase db, int daytimeId, int beginHour, int beginMinute) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("UPDATE daytime SET begin_hour=" + beginHour + ", " + "begin_minute=" + beginMinute + " WHERE " + "daytimeID=" + daytimeId);
    }

    public static void updateDaytimeEnd(SQLiteDatabase db, int daytimeId, int endHour, int endMinute) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("UPDATE daytime SET end_hour=" + endHour + ", " + "end_minute=" + endMinute + " WHERE " + "daytimeID=" + daytimeId);
    }

    public static void updateDaytime(SQLiteDatabase db, int daytimeId, String daytimeName) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("UPDATE daytime SET daytimename='" + daytimeName + "' WHERE " + "daytimeID=" + daytimeId);
    }

    public static void insertLog(SQLiteDatabase db, String actionType, String actionString) {
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL("INSERT INTO log (timestamp, actiontype, actionstring) VALUES ('" + new Date() + "', '" + actionType + "', '" + actionString + "')");
    }
}
