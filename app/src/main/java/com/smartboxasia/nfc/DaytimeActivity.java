package com.smartboxasia.nfc;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Iterator;

public class DaytimeActivity extends Activity {
    boolean weg;

    /* renamed from: ch.zingge.ds_nfc.DaytimeActivity.10 */
    class AnonymousClass10 implements OnClickListener {
        private final /* synthetic */ Daytime val$daytime;
        private final /* synthetic */ LinearLayout val$daytimeTotalLayout;
        private final /* synthetic */ TimePicker val$tpd;

        AnonymousClass10(TimePicker timePicker, Daytime daytime, LinearLayout linearLayout) {
            this.val$tpd = timePicker;
            this.val$daytime = daytime;
            this.val$daytimeTotalLayout = linearLayout;
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            DbOps.updateDaytimeEnd(App.getDB(), this.val$daytime.getDaytimeId(), this.val$tpd.getCurrentHour().intValue(), this.val$tpd.getCurrentMinute().intValue());
            LinearLayout daytimeLayout = (LinearLayout) DaytimeActivity.this.findViewById(R.id.linearlayout_daytime);
            int positionOfThisTagLayout = daytimeLayout.indexOfChild(this.val$daytimeTotalLayout);
            DaytimeActivity.this.reloadDaytimes();
            DaytimeActivity.this.highlightViewWithAnimation((LinearLayout) daytimeLayout.getChildAt(positionOfThisTagLayout));
        }
    }

    /* renamed from: ch.zingge.ds_nfc.DaytimeActivity.1 */
    class C00251 implements AnimationListener {
        private final /* synthetic */ LinearLayout val$daytimeTotalLayout;

        C00251(LinearLayout linearLayout) {
            this.val$daytimeTotalLayout = linearLayout;
        }

        public void onAnimationEnd(Animation animation) {
            this.val$daytimeTotalLayout.setVisibility(View.GONE);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    /* renamed from: ch.zingge.ds_nfc.DaytimeActivity.2 */
    class C00262 implements AnimationListener {
        private final /* synthetic */ Animation val$add;
        private final /* synthetic */ LinearLayout val$daytimeTotalLayout;

        C00262(LinearLayout linearLayout, Animation animation) {
            this.val$daytimeTotalLayout = linearLayout;
            this.val$add = animation;
        }

        public void onAnimationEnd(Animation animation) {
            this.val$daytimeTotalLayout.startAnimation(this.val$add);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    /* renamed from: ch.zingge.ds_nfc.DaytimeActivity.3 */
    class C00273 implements OnLongClickListener {
        private final /* synthetic */ Daytime val$daytime;
        private final /* synthetic */ LinearLayout val$daytimeTotalLayoutWithBack;

        C00273(Daytime daytime, LinearLayout linearLayout) {
            this.val$daytime = daytime;
            this.val$daytimeTotalLayoutWithBack = linearLayout;
        }

        public boolean onLongClick(View v) {
            DaytimeActivity.this.showDaytimeOptionsDialog(this.val$daytime, this.val$daytimeTotalLayoutWithBack);
            return false;
        }
    }

    /* renamed from: ch.zingge.ds_nfc.DaytimeActivity.4 */
    class C00284 implements OnLongClickListener {
        private final /* synthetic */ Daytime val$daytime;
        private final /* synthetic */ LinearLayout val$daytimeTotalLayout;

        C00284(Daytime daytime, LinearLayout linearLayout) {
            this.val$daytime = daytime;
            this.val$daytimeTotalLayout = linearLayout;
        }

        public boolean onLongClick(View v) {
            DaytimeActivity.this.showDaytimeOptionsDialog(this.val$daytime, this.val$daytimeTotalLayout);
            return false;
        }
    }

    /* renamed from: ch.zingge.ds_nfc.DaytimeActivity.5 */
    class C00295 implements OnClickListener {
        private final /* synthetic */ Daytime val$daytime;
        private final /* synthetic */ LinearLayout val$daytimeTotalLayout;

        C00295(Daytime daytime, LinearLayout linearLayout) {
            this.val$daytime = daytime;
            this.val$daytimeTotalLayout = linearLayout;
        }

        public void onClick(DialogInterface dialog, int which) {
            if (which == 0) {
                DbOps.deleteDaytime(App.getDB(), this.val$daytime.getDaytimeId());
                DaytimeActivity.this.removeViewWithAnimation(this.val$daytimeTotalLayout);
            }
            if (which == 1) {
                DaytimeActivity.this.showDaytimeRenameDialog(this.val$daytime, this.val$daytimeTotalLayout);
            }
            if (which == 2) {
                DaytimeActivity.this.showDaytimePickerStart(this.val$daytime, this.val$daytimeTotalLayout);
            }
            if (which == 3) {
                DaytimeActivity.this.showDaytimePickerStop(this.val$daytime, this.val$daytimeTotalLayout);
            }
        }
    }

    /* renamed from: ch.zingge.ds_nfc.DaytimeActivity.6 */
    class C00306 implements OnClickListener {
        private final /* synthetic */ Daytime val$daytime;
        private final /* synthetic */ LinearLayout val$daytimeTotalLayout;
        private final /* synthetic */ EditText val$input;

        C00306(EditText editText, Daytime daytime, LinearLayout linearLayout) {
            this.val$input = editText;
            this.val$daytime = daytime;
            this.val$daytimeTotalLayout = linearLayout;
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            DbOps.updateDaytime(App.getDB(), this.val$daytime.getDaytimeId(), this.val$input.getText().toString());
            LinearLayout daytimeLayout = (LinearLayout) DaytimeActivity.this.findViewById(R.id.linearlayout_daytime);
            int positionOfThisTagLayout = daytimeLayout.indexOfChild(this.val$daytimeTotalLayout);
            DaytimeActivity.this.reloadDaytimes();
            DaytimeActivity.this.highlightViewWithAnimation((LinearLayout) daytimeLayout.getChildAt(positionOfThisTagLayout));
        }
    }

    /* renamed from: ch.zingge.ds_nfc.DaytimeActivity.7 */
    class C00317 implements OnClickListener {
        C00317() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
        }
    }

    /* renamed from: ch.zingge.ds_nfc.DaytimeActivity.8 */
    class C00328 implements OnClickListener {
        private final /* synthetic */ Daytime val$daytime;
        private final /* synthetic */ LinearLayout val$daytimeTotalLayout;
        private final /* synthetic */ TimePicker val$tpd;

        C00328(TimePicker timePicker, Daytime daytime, LinearLayout linearLayout) {
            this.val$tpd = timePicker;
            this.val$daytime = daytime;
            this.val$daytimeTotalLayout = linearLayout;
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            DbOps.updateDaytimeBegin(App.getDB(), this.val$daytime.getDaytimeId(), this.val$tpd.getCurrentHour().intValue(), this.val$tpd.getCurrentMinute().intValue());
            LinearLayout daytimeLayout = (LinearLayout) DaytimeActivity.this.findViewById(R.id.linearlayout_daytime);
            int positionOfThisTagLayout = daytimeLayout.indexOfChild(this.val$daytimeTotalLayout);
            DaytimeActivity.this.reloadDaytimes();
            DaytimeActivity.this.highlightViewWithAnimation((LinearLayout) daytimeLayout.getChildAt(positionOfThisTagLayout));
        }
    }

    /* renamed from: ch.zingge.ds_nfc.DaytimeActivity.9 */
    class C00339 implements OnClickListener {
        C00339() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
        }
    }

    public DaytimeActivity() {
        this.weg = false;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.activity_daytime);
        App.getActivities().add(this);
        LinearLayout llDaytimes = (LinearLayout) findViewById(R.id.linearlayout_daytime);
        if (collectDaytimes().size() > 0) {
            Iterator it = collectDaytimes().iterator();
            while (it.hasNext()) {
                llDaytimes.addView(createDaytimeLayout((Daytime) it.next()));
            }
            return;
        }
        TextView tv = new TextView(this);
        tv.setText(getString(R.string.text_tageszeiten_keine_vorhanden) + ".");
        tv.setWidth(-1);
        tv.setGravity(17);
        tv.setTextSize(14.0f);
        tv.setPadding(20, 10, 10, 20);
        tv.setBackgroundColor(-1);
        llDaytimes.addView(tv);
    }

    public void onBackPressed() {
        finish();
    }

    public ArrayList<Daytime> collectDaytimes() {
        ArrayList<Daytime> daytimes = new ArrayList();
        Cursor cDaytimes = App.getDB().rawQuery("SELECT * FROM daytime WHERE daytimeID <> 1", null);
        while (cDaytimes.moveToNext()) {
            Daytime daytime = new Daytime();
            daytime.setDaytimeId(cDaytimes.getInt(0));
            daytime.setName(cDaytimes.getString(1));
            daytime.setBeginHour(cDaytimes.getInt(2));
            daytime.setBeginMin(cDaytimes.getInt(3));
            daytime.setEndHour(cDaytimes.getInt(4));
            daytime.setEndMin(cDaytimes.getInt(5));
            daytimes.add(daytime);
        }
        return daytimes;
    }

    public void removeViewWithAnimation(LinearLayout daytimeTotalLayout) {
        Animation remove = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        daytimeTotalLayout.startAnimation(remove);
        remove.setAnimationListener(new C00251(daytimeTotalLayout));
    }

    public void highlightViewWithAnimation(LinearLayout daytimeTotalLayout) {
        Animation remove = AnimationUtils.loadAnimation(this, R.anim.highlight1);
        Animation add = AnimationUtils.loadAnimation(this, R.anim.highlight2);
        daytimeTotalLayout.startAnimation(remove);
        remove.setAnimationListener(new C00262(daytimeTotalLayout, add));
    }

    public void onDaytimeAddClick(View view) {
        DbOps.insertDaytime(App.getDB(), getString(R.string.text_tageszeiten_neu), 0, 0, 0, 0);
        LinearLayout llDaytimes = (LinearLayout) findViewById(R.id.linearlayout_daytime);
        llDaytimes.removeAllViews();
        LinearLayout llNewDaytime = null;
        Iterator it = collectDaytimes().iterator();
        while (it.hasNext()) {
            llNewDaytime = createDaytimeLayout((Daytime) it.next());
            llDaytimes.addView(llNewDaytime);
        }
        highlightViewWithAnimation(llNewDaytime);
    }

    public void reloadDaytimes() {
        LinearLayout llDaytimes = (LinearLayout) findViewById(R.id.linearlayout_daytime);
        llDaytimes.removeAllViews();
        if (collectDaytimes().size() > 0) {
            Iterator it = collectDaytimes().iterator();
            while (it.hasNext()) {
                llDaytimes.addView(createDaytimeLayout((Daytime) it.next()));
            }
            return;
        }
        TextView tv = new TextView(this);
        tv.setText("2131165198.");
        tv.setWidth(-1);
        tv.setGravity(17);
        tv.setTextSize(14.0f);
        tv.setPadding(20, 10, 10, 20);
        tv.setBackgroundColor(-1);
        llDaytimes.addView(tv);
    }

    public void reloadDaytimes(View view) {
        reloadDaytimes();
    }

    public LinearLayout createDaytimeLayout(Daytime daytime) {
        LinearLayout daytimeTotalLayout = new LinearLayout(this);
        int daytimeId = daytime.getDaytimeId();
        String daytimeName = daytime.getName();
        String daytimeBegin = setTimeFormat(new StringBuilder(String.valueOf(Integer.toString(daytime.getBeginHour()))).append("=").append(Integer.toString(daytime.getBeginMin())).toString());
        String daytimeEnd = setTimeFormat(new StringBuilder(String.valueOf(Integer.toString(daytime.getEndHour()))).append("=").append(Integer.toString(daytime.getEndMin())).toString());
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        MarginLayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.setMargins(changeDpToPx(10), changeDpToPx(10), changeDpToPx(10), 0);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundColor(Color.rgb(0, 139, 0));
        linearLayout.setId(daytimeId);
        daytimeTotalLayout.setOrientation(LinearLayout.HORIZONTAL);
        MarginLayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
        layoutParams2.setMargins(changeDpToPx(10), changeDpToPx(10), changeDpToPx(10), 0);
        daytimeTotalLayout.setLayoutParams(layoutParams2);
        daytimeTotalLayout.setBackgroundResource(R.drawable.button);
        daytimeTotalLayout.setId(daytimeId);
        daytimeTotalLayout.setPadding(10, 20, 20, 20);
        LinearLayout daytimeImageLayout = new LinearLayout(this);
        daytimeImageLayout.setOrientation(LinearLayout.VERTICAL);
        daytimeImageLayout.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        LinearLayout daytimeTextLayout = new LinearLayout(this);
        daytimeTextLayout.setOrientation(LinearLayout.VERTICAL);
        daytimeTextLayout.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 1.0f));
        daytimeTextLayout.setBackgroundColor(Color.rgb(180, 180, 180));
        LinearLayout daytimeTextLayoutOben = new LinearLayout(this);
        daytimeTextLayoutOben.setOrientation(LinearLayout.HORIZONTAL);
        MarginLayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams3.bottomMargin = 1;
        daytimeTextLayoutOben.setLayoutParams(layoutParams3);
        daytimeTextLayoutOben.setBackgroundResource(R.drawable.button);
        LinearLayout daytimeTextLayoutTitel = new LinearLayout(this);
        daytimeTextLayoutTitel.setOrientation(LinearLayout.VERTICAL);
        daytimeTextLayoutTitel.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        daytimeTextLayoutTitel.setPadding(5, 0, 5, 0);
        LinearLayout daytimeTextLayoutAktiv = new LinearLayout(this);
        daytimeTextLayoutAktiv.setOrientation(LinearLayout.VERTICAL);
        daytimeTextLayoutAktiv.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        daytimeTextLayoutAktiv.setPadding(5, 0, 5, 0);
        new LinearLayout.LayoutParams(-1, -2).bottomMargin = 1;
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.day32);
        imageView.setPadding(0, 0, 30, 0);
        imageView.setVisibility(View.VISIBLE);
        TextView textView1 = new TextView(this);
        TextView textView2 = new TextView(this);
        TextView textView3 = new TextView(this);
        textView1.setText(daytimeName);
        textView1.setTypeface(null, 1);
        textView1.setTextSize(14.0f);
        textView1.setBackgroundResource(R.drawable.button);
        textView1.setWidth(-2);
        textView1.setSingleLine(true);
        textView1.setEllipsize(TruncateAt.END);
        textView1.setVisibility(View.VISIBLE);
        if (daytimeBegin.equals(daytimeEnd)) {
            textView2.setText(new StringBuilder(String.valueOf(daytimeBegin)).append("  -  ").append(daytimeEnd).toString());
            textView2.setTextColor(Color.RED);
        } else {
            textView2.setText(new StringBuilder(String.valueOf(daytimeBegin)).append("  -  ").append(daytimeEnd).toString());
        }
        textView2.setTextSize(12.0f);
        textView2.setPadding(5, 0, 5, 0);
        textView2.setBackgroundResource(R.drawable.button);
        textView2.setWidth(-2);
        textView2.setVisibility(View.VISIBLE);
        if (daytimeId == CheckOps.getActualDaytimeId()) {
            int text_tageszeiten_zz_aktiv = R.string.text_tageszeiten_zz_aktiv;
            textView3.setText(getString(text_tageszeiten_zz_aktiv));
            textView3.setTextSize(14.0f);
            textView3.setBackgroundResource(R.drawable.button);
            textView3.setWidth(-2);
            textView3.setGravity(5);
            textView3.setVisibility(View.VISIBLE);
        }
        daytimeTotalLayout.addView(daytimeImageLayout);
        daytimeTotalLayout.addView(daytimeTextLayout);
        daytimeImageLayout.addView(imageView);
        daytimeTextLayout.addView(daytimeTextLayoutOben);
        daytimeTextLayout.addView(textView1);
        daytimeTextLayoutOben.addView(daytimeTextLayoutTitel);
        daytimeTextLayoutOben.addView(daytimeTextLayoutAktiv);
        daytimeTextLayoutTitel.addView(textView2);
        daytimeTextLayoutAktiv.addView(textView3);
        if (daytimeId == CheckOps.getActualDaytimeId()) {
            layoutParams.setMargins(changeDpToPx(0), changeDpToPx(0), changeDpToPx(0), changeDpToPx(0));
            daytimeTotalLayout.setLayoutParams(layoutParams);
            linearLayout.addView(daytimeTotalLayout);
            imageView.setImageResource(R.drawable.day32y);
            linearLayout.setOnLongClickListener(new C00273(daytime, linearLayout));
            return linearLayout;
        }
        daytimeTotalLayout.setOnLongClickListener(new C00284(daytime, daytimeTotalLayout));
        return daytimeTotalLayout;
    }

    private int changeDpToPx(int dp) {
        return (int) (((double) (((float) dp) * getResources().getDisplayMetrics().density)) + 0.5d);
    }

    public String setTimeFormat(String time) {
        String minute = time.split("=")[1];
        String hour = time.split("=")[0];
        if (minute.equals("1") || minute.equals("2") || minute.equals("3") || minute.equals("4") || minute.equals("5") || minute.equals("6") || minute.equals("7") || minute.equals("8") || minute.equals("9") || minute.equals("0")) {
            minute = "0" + minute;
        }
        if (hour.equals("1") || hour.equals("2") || hour.equals("3") || hour.equals("4") || hour.equals("5") || hour.equals("6") || hour.equals("7") || hour.equals("8") || hour.equals("9") || hour.equals("0")) {
            hour = "0" + hour;
        }
        return new StringBuilder(String.valueOf(hour)).append(":").append(minute).toString();
    }

    public void showDaytimeOptionsDialog(Daytime daytime, LinearLayout daytimeTotalLayout) {
        Builder optionsDialog = new Builder(this);
        optionsDialog.setItems(new String[]{getString(R.string.menu_text_tageszeiten_loeschen), getString(R.string.menu_text_tageszeiten_umbenennen), getString(R.string.menu_text_tageszeiten_start), getString(R.string.menu_text_tageszeiten_ende)}, new C00295(daytime, daytimeTotalLayout));
        optionsDialog.show();
    }

    public void showDaytimeRenameDialog(Daytime daytime, LinearLayout daytimeTotalLayout) {
        Builder daytimeInputDialog = new Builder(this);
        daytimeInputDialog.setTitle(getString(R.string.dialog_tageszeiten_umbenennen));
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        TextView title = new TextView(this);
        title.setText(getString(R.string.dialog_tageszeiten_umbenennen_name));
        title.setTextColor(Color.LTGRAY);
        title.setPadding(10, 0, 0, 0);
        EditText input = new EditText(this);
        input.setText(daytime.getName());
        ll.addView(title);
        ll.addView(input);
        daytimeInputDialog.setView(ll);
        daytimeInputDialog.setPositiveButton(getString(R.string.button_text_speichern), new C00306(input, daytime, daytimeTotalLayout));
        daytimeInputDialog.setNegativeButton(getString(R.string.button_text_abbrechen), new C00317());
        daytimeInputDialog.show();
    }

    public void showDaytimePickerStart(Daytime daytime, LinearLayout daytimeTotalLayout) {
        Cursor cDaytimeStart = App.getDB().rawQuery("SELECT begin_hour, begin_minute FROM daytime WHERE daytimeID=" + daytime.getDaytimeId(), null);
        cDaytimeStart.moveToFirst();
        int hour = cDaytimeStart.getInt(0);
        int minute = cDaytimeStart.getInt(1);
        Builder daytimeInputDialog = new Builder(this);
        daytimeInputDialog.setTitle(getString(R.string.dialog_tageszeiten_startzeitpunkt) + " '" + daytime.getName() + "'");
        TimePicker tpd = new TimePicker(this);
        tpd.setIs24HourView(Boolean.valueOf(true));
        tpd.setCurrentHour(Integer.valueOf(hour));
        tpd.setCurrentMinute(Integer.valueOf(minute));
        daytimeInputDialog.setView(tpd);
        daytimeInputDialog.setPositiveButton(getString(R.string.button_text_speichern), new C00328(tpd, daytime, daytimeTotalLayout));
        daytimeInputDialog.setNegativeButton(getString(R.string.button_text_abbrechen), new C00339());
        daytimeInputDialog.show();
    }

    public void showDaytimePickerStop(Daytime daytime, LinearLayout daytimeTotalLayout) {
        Cursor cDaytimeStart = App.getDB().rawQuery("SELECT end_hour, end_minute FROM daytime WHERE daytimeID=" + daytime.getDaytimeId(), null);
        cDaytimeStart.moveToFirst();
        int hour = cDaytimeStart.getInt(0);
        int minute = cDaytimeStart.getInt(1);
        Builder daytimeInputDialog = new Builder(this);
        daytimeInputDialog.setTitle(getString(R.string.dialog_tageszeiten_endzeitpunkt) + " '" + daytime.getName() + "'");
        TimePicker tpd = new TimePicker(this);
        tpd.setIs24HourView(Boolean.valueOf(true));
        tpd.setCurrentHour(Integer.valueOf(hour));
        tpd.setCurrentMinute(Integer.valueOf(minute));
        daytimeInputDialog.setView(tpd);
        daytimeInputDialog.setPositiveButton(getString(R.string.button_text_speichern), new AnonymousClass10(tpd, daytime, daytimeTotalLayout));
        daytimeInputDialog.setNegativeButton(getString(R.string.button_text_abbrechen), new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        daytimeInputDialog.show();
    }

    public void onResume() {
        super.onResume();
        enableNfcWrite();
    }

    private void enableNfcWrite() {
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter != null) {
            nfcAdapter.enableForegroundDispatch(this, getPendingIntent(), new IntentFilter[]{new IntentFilter("android.nfc.action.TAG_DISCOVERED")}, null);
        }
    }

    private PendingIntent getPendingIntent() {
        return PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(536870912), 0);
    }

    public void onNewIntent(Intent intent) {
        Toast.makeText(this, getString(R.string.notification_toast_seite_wechseln), Toast.LENGTH_LONG).show();
    }
}
