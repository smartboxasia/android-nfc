package com.smartboxasia.nfc;

public class NfcTag {
    private String nfctagHexId;
    private String nfctagHint;
    private int nfctagId;
    private String nfctagName;
    private int ruleDaytime;

    public int getNfctagId() {
        return this.nfctagId;
    }

    public void setNfctagId(int nfctagId) {
        this.nfctagId = nfctagId;
    }

    public String getNfctagHexId() {
        return this.nfctagHexId;
    }

    public void setNfctagHexId(String nfctagHexId) {
        this.nfctagHexId = nfctagHexId;
    }

    public String getNfctagName() {
        return this.nfctagName;
    }

    public void setNfctagName(String nfctagName) {
        this.nfctagName = nfctagName;
    }

    public String getNfctagHint() {
        return this.nfctagHint;
    }

    public void setNfctagHint(String nfctagHint) {
        this.nfctagHint = nfctagHint;
    }

    public int getRuleDaytime() {
        return this.ruleDaytime;
    }

    public void setRuleDaytime(int ruleDaytime) {
        this.ruleDaytime = ruleDaytime;
    }
}
