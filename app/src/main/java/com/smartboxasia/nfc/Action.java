package com.smartboxasia.nfc;

public class Action {
    private int actionId;
    private int daytime;
    private int nfctag;
    private String sceneName;
    private int season;
    private int standardaction;
    private String timestamp;

    public String getSceneName() {
        return this.sceneName;
    }

    public void setSceneName(String sceneName) {
        this.sceneName = sceneName;
    }

    public int getActionId() {
        return this.actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public int getSeason() {
        return this.season;
    }

    public void setSeason(int season) {
        this.season = season;
    }

    public int getDaytime() {
        return this.daytime;
    }

    public void setDaytime(int daytime) {
        this.daytime = daytime;
    }

    public int getNfctag() {
        return this.nfctag;
    }

    public void setNfctag(int nfctag) {
        this.nfctag = nfctag;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getStandardaction() {
        return this.standardaction;
    }

    public void setStandardaction(int standardaction) {
        this.standardaction = standardaction;
    }
}
