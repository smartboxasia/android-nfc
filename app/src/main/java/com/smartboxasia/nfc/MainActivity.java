package com.smartboxasia.nfc;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.provider.Settings.System;
import android.support.v4.view.MotionEventCompat;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends Activity {
    private int actualDaytimeId;
    private String actualNfcHexId;
    private NfcTag actualNfcTag;
    private int actualOrientationId;
    private LinearLayout actualTagLayout;
    private boolean comeFromdSApp;
    private boolean dsTagCreatable;

    /* renamed from: ch.zingge.ds_nfc.MainActivity.10 */
    class AnonymousClass10 implements AnimationListener {
        private final /* synthetic */ Animation val$add;
        private final /* synthetic */ LinearLayout val$linearLayout;

        AnonymousClass10(LinearLayout linearLayout, Animation animation) {
            this.val$linearLayout = linearLayout;
            this.val$add = animation;
        }

        public void onAnimationEnd(Animation animation) {
            this.val$linearLayout.startAnimation(this.val$add);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.11 */
    class AnonymousClass11 implements AnimationListener {
        private final /* synthetic */ LinearLayout val$nfctagTotalLayout;

        AnonymousClass11(LinearLayout linearLayout) {
            this.val$nfctagTotalLayout = linearLayout;
        }

        public void onAnimationEnd(Animation animation) {
            this.val$nfctagTotalLayout.setVisibility(View.GONE);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.12 */
    class AnonymousClass12 implements OnClickListener {
        private final /* synthetic */ EditText val$input;
        private final /* synthetic */ EditText val$input2;
        private final /* synthetic */ LinearLayout val$nfcTagLayout;
        private final /* synthetic */ NfcTag val$nfctag;

        AnonymousClass12(EditText editText, EditText editText2, NfcTag nfcTag, LinearLayout linearLayout) {
            this.val$input = editText;
            this.val$input2 = editText2;
            this.val$nfctag = nfcTag;
            this.val$nfcTagLayout = linearLayout;
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            String newNfctagName = this.val$input.getText().toString();
            String newNfctagHint = this.val$input2.getText().toString();
            if (newNfctagName.equals("")) {
                Toast.makeText(MainActivity.this.getBaseContext(), MainActivity.this.getString(R.string.notification_toast_nfctagname_ungueltig), Toast.LENGTH_SHORT).show();
                return;
            }
            DbOps.updateNfctag(App.getDB(), this.val$nfctag.getNfctagHexId(), newNfctagName, newNfctagHint);
            LinearLayout daytimeLayout = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
            int positionOfThisTagLayout = daytimeLayout.indexOfChild(this.val$nfcTagLayout);
            MainActivity.this.reloadNfctags();
            MainActivity.this.highlightViewWithAnimation((LinearLayout) daytimeLayout.getChildAt(positionOfThisTagLayout));
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.14 */
    class AnonymousClass14 implements OnClickListener {
        private final /* synthetic */ NfcTag val$nfctag;
        private final /* synthetic */ RadioGroup val$radiogroup;
        private final /* synthetic */ LinearLayout val$tagLayout;

        AnonymousClass14(RadioGroup radioGroup, NfcTag nfcTag, LinearLayout linearLayout) {
            this.val$radiogroup = radioGroup;
            this.val$nfctag = nfcTag;
            this.val$tagLayout = linearLayout;
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            if (this.val$radiogroup.getCheckedRadioButtonId() != -1) {
                MainActivity.this.actualNfcTag = this.val$nfctag;
                MainActivity.this.actualDaytimeId = this.val$radiogroup.getCheckedRadioButtonId();
                MainActivity.this.actualTagLayout = this.val$tagLayout;
                MainActivity.this.setupIntent(CheckOps.getNewActionId());
                return;
            }
            Toast.makeText(MainActivity.this.getBaseContext(), MainActivity.this.getString(R.string.notification_toast_keine_tageszeit_ausgewaehlt), Toast.LENGTH_SHORT).show();
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.16 */
    class AnonymousClass16 implements OnClickListener {
        private final /* synthetic */ NfcTag val$nfctag;
        private final /* synthetic */ LinearLayout val$tagLayout;

        AnonymousClass16(LinearLayout linearLayout, NfcTag nfcTag) {
            this.val$tagLayout = linearLayout;
            this.val$nfctag = nfcTag;
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            dialog.cancel();
            LinearLayout ll = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
            int positionOfThisTagLayout = ll.indexOfChild(this.val$tagLayout);
            ll.removeView(this.val$tagLayout);
            ll.addView(MainActivity.this.createTagLayout(this.val$nfctag), positionOfThisTagLayout);
            MainActivity.this.highlightViewWithAnimation((LinearLayout) ll.getChildAt(positionOfThisTagLayout));
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.17 */
    class AnonymousClass17 implements OnClickListener {
        private final /* synthetic */ NfcTag val$nfctag;
        private final /* synthetic */ LinearLayout val$tagLayout;

        AnonymousClass17(LinearLayout linearLayout, NfcTag nfcTag) {
            this.val$tagLayout = linearLayout;
            this.val$nfctag = nfcTag;
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            dialog.cancel();
            LinearLayout ll = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
            int positionOfThisTagLayout = ll.indexOfChild(this.val$tagLayout);
            ll.removeView(this.val$tagLayout);
            ll.addView(MainActivity.this.createTagLayout(this.val$nfctag), positionOfThisTagLayout);
            MainActivity.this.highlightViewWithAnimation((LinearLayout) ll.getChildAt(positionOfThisTagLayout));
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.1 */
    class C00341 implements OnClickListener {
        private final /* synthetic */ EditText val$input;
        private final /* synthetic */ EditText val$input2;
        private final /* synthetic */ String val$nfctag_hexID;

        C00341(EditText editText, EditText editText2, String str) {
            this.val$input = editText;
            this.val$input2 = editText2;
            this.val$nfctag_hexID = str;
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            String tagName = this.val$input.getText().toString();
            String tagHint = this.val$input2.getText().toString();
            if (tagName.length() <= 0 || tagName.contains("=") || tagHint.contains("=")) {
                Toast.makeText(MainActivity.this.getBaseContext(), MainActivity.this.getString(R.string.notification_toast_nfctagname_ungueltig), Toast.LENGTH_LONG).show();
                return;
            }
            NfcTag lastScannedTag = new NfcTag();
            lastScannedTag.setNfctagName(tagName);
            LinearLayout llNfctags;
            LinearLayout llNewNfctag;
            Iterator it;
            if (tagHint.length() > 0) {
                DbOps.insertNfctag(App.getDB(), this.val$nfctag_hexID, tagName, tagHint, 0);
                lastScannedTag.setNfctagHint(tagHint);
                llNfctags = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
                llNfctags.removeAllViews();
                llNewNfctag = null;
                it = MainActivity.this.collectNfcTags().iterator();
                while (it.hasNext()) {
                    llNewNfctag = MainActivity.this.createTagLayout((NfcTag) it.next());
                    llNfctags.addView(llNewNfctag);
                }
                MainActivity.this.highlightViewWithAnimation(llNewNfctag);
            } else {
                DbOps.insertNfctag(App.getDB(), this.val$nfctag_hexID, tagName, "", 0);
                lastScannedTag.setNfctagHint("");
                llNfctags = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
                llNfctags.removeAllViews();
                llNewNfctag = null;
                it = MainActivity.this.collectNfcTags().iterator();
                while (it.hasNext()) {
                    llNewNfctag = MainActivity.this.createTagLayout((NfcTag) it.next());
                    llNfctags.addView(llNewNfctag);
                }
                MainActivity.this.highlightViewWithAnimation(llNewNfctag);
            }
            App.setLastScannedTag(lastScannedTag);
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.21 */
    class AnonymousClass21 implements AnimationListener {
        private final /* synthetic */ LinearLayout val$llNew;
        private final /* synthetic */ LinearLayout val$llOld;

        AnonymousClass21(LinearLayout linearLayout, LinearLayout linearLayout2) {
            this.val$llOld = linearLayout;
            this.val$llNew = linearLayout2;
        }

        public void onAnimationEnd(Animation animation) {
            this.val$llOld.setVisibility(View.GONE);
            this.val$llNew.startAnimation(AnimationUtils.loadAnimation(MainActivity.this.getBaseContext(), R.anim.fadein_short));
            this.val$llNew.setVisibility(View.VISIBLE);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.2 */
    class C00352 implements OnClickListener {
        C00352() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.3 */
    class C00363 implements OnLongClickListener {
        private final /* synthetic */ NfcTag val$nfctag;
        private final /* synthetic */ LinearLayout val$tagTotalLayoutWithBack;

        C00363(NfcTag nfcTag, LinearLayout linearLayout) {
            this.val$nfctag = nfcTag;
            this.val$tagTotalLayoutWithBack = linearLayout;
        }

        public boolean onLongClick(View v) {
            MainActivity.this.showTagOptionsDialog(this.val$nfctag, this.val$tagTotalLayoutWithBack);
            return false;
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.4 */
    class C00374 implements OnLongClickListener {
        private final /* synthetic */ NfcTag val$nfctag;
        private final /* synthetic */ LinearLayout val$tagTotalLayout;

        C00374(NfcTag nfcTag, LinearLayout linearLayout) {
            this.val$nfctag = nfcTag;
            this.val$tagTotalLayout = linearLayout;
        }

        public boolean onLongClick(View v) {
            MainActivity.this.showTagOptionsDialog(this.val$nfctag, this.val$tagTotalLayout);
            return false;
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.5 */
    class C00385 implements OnClickListener {
        private final /* synthetic */ NfcTag val$nfctag;
        private final /* synthetic */ LinearLayout val$tagLayout;

        C00385(NfcTag nfcTag, LinearLayout linearLayout) {
            this.val$nfctag = nfcTag;
            this.val$tagLayout = linearLayout;
        }

        public void onClick(DialogInterface dialog, int which) {
            if (which == 0) {
                DbOps.deleteNfctag(App.getDB(), this.val$nfctag.getNfctagHexId());
                MainActivity.this.removeViewWithAnimation(this.val$tagLayout);
            }
            if (which == 1) {
                MainActivity.this.showNfctagRenameDialog(this.val$nfctag, this.val$tagLayout);
            }
            if (which == 2) {
                MainActivity.this.actualOrientationId = 0;
                MainActivity.this.showNfctagLinkerDialog(this.val$nfctag, this.val$tagLayout);
            }
            if (which == 3) {
                MainActivity.this.actualOrientationId = 1;
                MainActivity.this.showNfctagLinkerDialog(this.val$nfctag, this.val$tagLayout);
            }
            LinearLayout ll;
            int positionOfThisTagLayout;
            if (which == 4) {
                DbOps.deleteAction(App.getDB(), this.val$nfctag.getNfctagId());
                ll = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
                positionOfThisTagLayout = ll.indexOfChild(this.val$tagLayout);
                ll.removeView(this.val$tagLayout);
                ll.addView(MainActivity.this.createTagLayout(this.val$nfctag), positionOfThisTagLayout);
                MainActivity.this.highlightViewWithAnimation((LinearLayout) ll.getChildAt(positionOfThisTagLayout));
            }
            if (which == 5) {
                DbOps.updateNfctag(App.getDB(), this.val$nfctag.getNfctagHexId(), 0);
                ll = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
                positionOfThisTagLayout = ll.indexOfChild(this.val$tagLayout);
                ll.removeView(this.val$tagLayout);
                ll.addView(MainActivity.this.createTagLayout(this.val$nfctag), positionOfThisTagLayout);
                MainActivity.this.highlightViewWithAnimation((LinearLayout) ll.getChildAt(positionOfThisTagLayout));
            }
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.6 */
    class C00396 implements OnClickListener {
        private final /* synthetic */ NfcTag val$nfctag;
        private final /* synthetic */ LinearLayout val$tagLayout;

        C00396(NfcTag nfcTag, LinearLayout linearLayout) {
            this.val$nfctag = nfcTag;
            this.val$tagLayout = linearLayout;
        }

        public void onClick(DialogInterface dialog, int which) {
            if (which == 0) {
                DbOps.deleteNfctag(App.getDB(), this.val$nfctag.getNfctagHexId());
                MainActivity.this.removeViewWithAnimation(this.val$tagLayout);
            }
            if (which == 1) {
                MainActivity.this.showNfctagRenameDialog(this.val$nfctag, this.val$tagLayout);
            }
            if (which == 2) {
                MainActivity.this.showNfctagLinkerDialog(this.val$nfctag, this.val$tagLayout);
            }
            LinearLayout ll;
            int positionOfThisTagLayout;
            if (which == 3) {
                DbOps.deleteAction(App.getDB(), this.val$nfctag.getNfctagId());
                ll = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
                positionOfThisTagLayout = ll.indexOfChild(this.val$tagLayout);
                ll.removeView(this.val$tagLayout);
                ll.addView(MainActivity.this.createTagLayout(this.val$nfctag), positionOfThisTagLayout);
                MainActivity.this.highlightViewWithAnimation((LinearLayout) ll.getChildAt(positionOfThisTagLayout));
            }
            if (which == 4) {
                DbOps.updateNfctag(App.getDB(), this.val$nfctag.getNfctagHexId(), 0);
                ll = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
                positionOfThisTagLayout = ll.indexOfChild(this.val$tagLayout);
                ll.removeView(this.val$tagLayout);
                ll.addView(MainActivity.this.createTagLayout(this.val$nfctag), positionOfThisTagLayout);
                MainActivity.this.highlightViewWithAnimation((LinearLayout) ll.getChildAt(positionOfThisTagLayout));
            }
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.7 */
    class C00407 implements OnClickListener {
        private final /* synthetic */ NfcTag val$nfctag;
        private final /* synthetic */ LinearLayout val$tagLayout;

        C00407(NfcTag nfcTag, LinearLayout linearLayout) {
            this.val$nfctag = nfcTag;
            this.val$tagLayout = linearLayout;
        }

        public void onClick(DialogInterface dialog, int which) {
            if (which == 0) {
                DbOps.deleteNfctag(App.getDB(), this.val$nfctag.getNfctagHexId());
                MainActivity.this.removeViewWithAnimation(this.val$tagLayout);
            }
            if (which == 1) {
                MainActivity.this.showNfctagRenameDialog(this.val$nfctag, this.val$tagLayout);
            }
            if (which == 2) {
                MainActivity.this.actualNfcTag = this.val$nfctag;
                MainActivity.this.actualDaytimeId = 1;
                MainActivity.this.actualTagLayout = this.val$tagLayout;
                MainActivity.this.actualOrientationId = 0;
                MainActivity.this.setupIntent(CheckOps.getNewActionId());
            }
            if (which == 3) {
                MainActivity.this.actualNfcTag = this.val$nfctag;
                MainActivity.this.actualDaytimeId = 1;
                MainActivity.this.actualTagLayout = this.val$tagLayout;
                MainActivity.this.actualOrientationId = 1;
                MainActivity.this.setupIntent(CheckOps.getNewActionId());
            }
            LinearLayout ll;
            int positionOfThisTagLayout;
            if (which == 4) {
                DbOps.deleteAction(App.getDB(), this.val$nfctag.getNfctagId());
                ll = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
                positionOfThisTagLayout = ll.indexOfChild(this.val$tagLayout);
                ll.removeView(this.val$tagLayout);
                ll.addView(MainActivity.this.createTagLayout(this.val$nfctag), positionOfThisTagLayout);
                MainActivity.this.highlightViewWithAnimation((LinearLayout) ll.getChildAt(positionOfThisTagLayout));
            }
            if (which == 5) {
                DbOps.updateNfctag(App.getDB(), this.val$nfctag.getNfctagHexId(), 1);
                ll = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
                positionOfThisTagLayout = ll.indexOfChild(this.val$tagLayout);
                ll.removeView(this.val$tagLayout);
                ll.addView(MainActivity.this.createTagLayout(this.val$nfctag), positionOfThisTagLayout);
                MainActivity.this.highlightViewWithAnimation((LinearLayout) ll.getChildAt(positionOfThisTagLayout));
            }
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.8 */
    class C00418 implements OnClickListener {
        private final /* synthetic */ NfcTag val$nfctag;
        private final /* synthetic */ LinearLayout val$tagLayout;

        C00418(NfcTag nfcTag, LinearLayout linearLayout) {
            this.val$nfctag = nfcTag;
            this.val$tagLayout = linearLayout;
        }

        public void onClick(DialogInterface dialog, int which) {
            if (which == 0) {
                DbOps.deleteNfctag(App.getDB(), this.val$nfctag.getNfctagHexId());
                MainActivity.this.removeViewWithAnimation(this.val$tagLayout);
            }
            if (which == 1) {
                MainActivity.this.showNfctagRenameDialog(this.val$nfctag, this.val$tagLayout);
            }
            if (which == 2) {
                MainActivity.this.actualNfcTag = this.val$nfctag;
                MainActivity.this.actualDaytimeId = 1;
                MainActivity.this.actualTagLayout = this.val$tagLayout;
                MainActivity.this.setupIntent(CheckOps.getNewActionId());
            }
            LinearLayout ll;
            int positionOfThisTagLayout;
            if (which == 3) {
                DbOps.deleteAction(App.getDB(), this.val$nfctag.getNfctagId());
                ll = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
                positionOfThisTagLayout = ll.indexOfChild(this.val$tagLayout);
                ll.removeView(this.val$tagLayout);
                ll.addView(MainActivity.this.createTagLayout(this.val$nfctag), positionOfThisTagLayout);
                MainActivity.this.highlightViewWithAnimation((LinearLayout) ll.getChildAt(positionOfThisTagLayout));
            }
            if (which == 4) {
                DbOps.updateNfctag(App.getDB(), this.val$nfctag.getNfctagHexId(), 1);
                ll = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_nfctags);
                positionOfThisTagLayout = ll.indexOfChild(this.val$tagLayout);
                ll.removeView(this.val$tagLayout);
                ll.addView(MainActivity.this.createTagLayout(this.val$nfctag), positionOfThisTagLayout);
                MainActivity.this.highlightViewWithAnimation((LinearLayout) ll.getChildAt(positionOfThisTagLayout));
            }
        }
    }

    /* renamed from: ch.zingge.ds_nfc.MainActivity.9 */
    class C00429 implements AnimationListener {
        private final /* synthetic */ Animation val$add;
        private final /* synthetic */ LinearLayout val$nfctagTotalLayout;

        C00429(LinearLayout linearLayout, Animation animation) {
            this.val$nfctagTotalLayout = linearLayout;
            this.val$add = animation;
        }

        public void onAnimationEnd(Animation animation) {
            this.val$nfctagTotalLayout.startAnimation(this.val$add);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    public MainActivity() {
        this.actualNfcHexId = "";
        this.actualOrientationId = 0;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.activity_main);
        App.getActivities().add(this);
        Iterator it = App.getActivities().iterator();
        while (it.hasNext()) {
            Activity activeActivity = (Activity) it.next();
            if (activeActivity != this) {
                activeActivity.finish();
            }
        }
        App.setLastScannedTag(null);
        this.dsTagCreatable = false;
        App.setDB(new DbOps(this).getWritableDatabase());
        App.setOrienationActivated(getSystemSettingRotation());
        if ("ch.zingge.ds_nfc.action.ENTER_WITH_UNKNOWN_TAG".equals(getIntent().getAction())) {
            showNfcTagInputDialog(getIntent().getExtras().getString("hexId"));
        }
        getIntent().getAction().equals("ch.zingge.ds_nfc.action.ENTER_WITH_KNOWN_TAG");
        LinearLayout llNfctags = (LinearLayout) findViewById(R.id.linearlayout_nfctags);
        if (collectNfcTags().size() > 0) {
            it = collectNfcTags().iterator();
            while (it.hasNext()) {
                llNfctags.addView(createTagLayout((NfcTag) it.next()));
            }
            return;
        }
        TextView tv = new TextView(this);
        tv.setText(getString(R.string.start_text_nfctags_erfassen));
        tv.setWidth(-1);
        tv.setGravity(17);
        tv.setTextSize(14.0f);
        tv.setPadding(20, 10, 10, 20);
        tv.setBackgroundColor(-1);
        llNfctags.addView(tv);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            String scene = data.getExtras().getString("com.smartboxasia.control.result.action");
            this.comeFromdSApp = true;
            DbOps.insertAction(App.getDB(), this.actualDaytimeId, this.actualNfcTag.getNfctagId(), scene, this.actualOrientationId);
            this.actualOrientationId = 0;
            if (this.actualNfcTag.getRuleDaytime() == 1) {
                showSceneConfirmationDialogWithActiveDaytime(scene, this.actualTagLayout, this.actualNfcTag);
            }
            if (this.actualNfcTag.getRuleDaytime() == 0) {
                showSceneConfirmationDialogWithoutActiveDaytime(scene, this.actualTagLayout, this.actualNfcTag);
            }
        }
    }

    public String changeIdtoHexString(byte[] id) {
        String hexId = "";
        for (byte idPart : id) {
            if (Integer.toHexString(idPart).length() > 2) {
                hexId = new StringBuilder(String.valueOf(Integer.toHexString(idPart).substring(6, 8))).append(hexId).toString();
            } else {
                hexId = Integer.toHexString(idPart) + hexId;
            }
        }
        return hexId;
    }

    public void onBackPressed() {
        Iterator it = App.getActivities().iterator();
        while (it.hasNext()) {
            ((Activity) it.next()).finish();
        }
    }

    private int changeDpToPx(int dp) {
        return (int) (((double) (((float) dp) * getResources().getDisplayMetrics().density)) + 0.5d);
    }

    public void startActivityDaytime(View view) {
        startActivity(new Intent(this, DaytimeActivity.class));
        App.setLastScannedTag(null);
    }

    public void setupIntent(String id) {
        Intent dsIntent = new Intent("com.smartboxasia.control.action.SETUP");
        dsIntent.putExtra("com.smartboxasia.control.callingPackage", getPackageName());
        dsIntent.putExtra("com.smartboxasia.control.actionIdentifier", id);
        Intent marketIntent = new Intent("android.intent.action.VIEW").setData(Uri.parse("market://search?q=pname:com.smartboxasia.control"));
        if (getPackageManager().queryIntentActivities(dsIntent, 0).size() > 0) {
            startActivityForResult(dsIntent, 0);
        } else {
            startActivity(marketIntent);
        }
    }

    public void callIntent(String id) {
        Intent serviceIntent = new Intent("com.smartboxasia.control.action.CALL");
        serviceIntent.putExtra("com.smartboxasia.control.callingPackage", getPackageName());
        serviceIntent.putExtra("com.smartboxasia.control.actionIdentifier", id);
        if (getPackageManager().queryIntentServices(serviceIntent, 0).size() > 0) {
            startService(serviceIntent);
        }
    }

    public boolean checkNfcTagExisting(String tagId) {
        App.getDB().setForeignKeyConstraintsEnabled(true);
        Cursor nfcTags = App.getDB().rawQuery("SELECT nfctag_hexID FROM nfctag", null);
        while (nfcTags.moveToNext()) {
            if (tagId.equals(nfcTags.getString(0))) {
                return true;
            }
        }
        return false;
    }

    public void showNfcTagInputDialog(String nfctag_hexID) {
        Builder nfcTagInputDialog = new Builder(this);
        nfcTagInputDialog.setMessage(getString(R.string.dialog_start_nfctag_neu_text));
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        EditText input = new EditText(this);
        input.setHint(getString(R.string.dialog_start_nfctag_neu_namensfeld));
        EditText input2 = new EditText(this);
        input2.setHint(getString(R.string.dialog_start_nfctag_neu_hinweisfeld));
        ll.addView(input);
        ll.addView(input2);
        nfcTagInputDialog.setView(ll);
        nfcTagInputDialog.setPositiveButton(getString(R.string.button_text_speichern), new C00341(input, input2, nfctag_hexID));
        nfcTagInputDialog.setNegativeButton(getString(R.string.button_text_speichern), new C00352());
        nfcTagInputDialog.show();
    }

    public ArrayList<NfcTag> collectNfcTags() {
        ArrayList<NfcTag> nfctags = new ArrayList();
        Cursor cNfcTags = App.getDB().rawQuery("SELECT * FROM nfctag", null);
        while (cNfcTags.moveToNext()) {
            NfcTag nfctag = new NfcTag();
            nfctag.setNfctagId(cNfcTags.getInt(0));
            nfctag.setNfctagHexId(cNfcTags.getString(1));
            nfctag.setNfctagName(cNfcTags.getString(2));
            nfctag.setNfctagHint(cNfcTags.getString(3));
            nfctag.setRuleDaytime(cNfcTags.getInt(4));
            nfctags.add(nfctag);
        }
        return nfctags;
    }

    public ArrayList<Daytime> collectDaytimes() {
        ArrayList<Daytime> daytimes = new ArrayList();
        Cursor cDaytimes = App.getDB().rawQuery("SELECT * FROM daytime WHERE daytimeID <> 1", null);
        while (cDaytimes.moveToNext()) {
            Daytime daytime = new Daytime();
            daytime.setDaytimeId(cDaytimes.getInt(0));
            daytime.setName(cDaytimes.getString(1));
            daytime.setBeginHour(cDaytimes.getInt(2));
            daytime.setBeginMin(cDaytimes.getInt(3));
            daytime.setEndHour(cDaytimes.getInt(4));
            daytime.setEndMin(cDaytimes.getInt(5));
            daytimes.add(daytime);
        }
        return daytimes;
    }

    public LinearLayout createTagLayout(NfcTag nfctag) {
        Iterator it;
        int nfctagId = nfctag.getNfctagId();
        String nfctagName = nfctag.getNfctagName();
        String nfctagHint = nfctag.getNfctagHint();
        Cursor cNfctag = App.getDB().rawQuery("SELECT rule_daytime FROM nfctag WHERE nfctagID=" + nfctag.getNfctagId(), null);
        cNfctag.moveToFirst();
        int nfctagDaytimeStatus = cNfctag.getInt(0);
        nfctag.setRuleDaytime(nfctagDaytimeStatus);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        MarginLayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.setMargins(changeDpToPx(10), changeDpToPx(10), changeDpToPx(10), 0);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundColor(Color.rgb(0, 139, 0));
        linearLayout.setId(nfctagId);
        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setOrientation(LinearLayout.HORIZONTAL);
        MarginLayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
        layoutParams2.setMargins(changeDpToPx(10), changeDpToPx(10), changeDpToPx(10), 0);
        linearLayout2.setLayoutParams(layoutParams2);
        linearLayout2.setBackgroundResource(R.drawable.button);
        linearLayout2.setId(nfctagId);
        linearLayout2.setPadding(10, 20, 20, 20);
        LinearLayout linearLayout3 = new LinearLayout(this);
        linearLayout3.setOrientation(LinearLayout.VERTICAL);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        LinearLayout linearLayout4 = new LinearLayout(this);
        linearLayout4.setOrientation(LinearLayout.VERTICAL);
        linearLayout4.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 1.0f));
        linearLayout4.setBackgroundColor(Color.rgb(180, 180, 180));
        LinearLayout.LayoutParams bottomMargin = new LinearLayout.LayoutParams(-1, -2);
        bottomMargin.bottomMargin = 1;
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.tag32);
        imageView.setPadding(0, 10, 30, 0);
        imageView.setVisibility(View.VISIBLE);
        TextView textView1 = new TextView(this);
        TextView textView2 = new TextView(this);
        if (nfctagHint.equals("")) {
            textView1.setText(nfctagName);
        } else {
            textView1.setText(new StringBuilder(String.valueOf(nfctagName)).append(" (").append(nfctagHint).append(")").toString());
        }
        textView1.setTypeface(null, 1);
        textView1.setTextSize(14.0f);
        textView1.setPadding(5, 0, 5, 0);
        textView1.setBackgroundResource(R.drawable.button);
        textView1.setLayoutParams(bottomMargin);
        textView1.setWidth(-2);
        textView1.setSingleLine(true);
        textView1.setEllipsize(TruncateAt.END);
        textView1.setVisibility(View.VISIBLE);
        if (nfctagDaytimeStatus == 1) {
            textView2.setText(getString(R.string.text_nfctags_tageszeiten_aktiviert));
            textView2.setTextColor(Color.rgb(0, 139, 0));
        } else {
            textView2.setText(getString(R.string.text_nfctags_tageszeiten_deaktiviert));
            textView2.setTextColor(Color.rgb(130, 130, 130));
        }
        textView2.setTextSize(12.0f);
        textView2.setPadding(5, 0, 5, 0);
        textView2.setBackgroundResource(R.drawable.button);
        textView2.setLayoutParams(bottomMargin);
        textView2.setWidth(-2);
        textView2.setVisibility(View.VISIBLE);
        linearLayout2.addView(linearLayout3);
        linearLayout2.addView(linearLayout4);
        linearLayout2.addView(imageView);
        linearLayout2.addView(textView1);
        linearLayout2.addView(textView2);
        // LinearLayout linearLayout = new LinearLayout(this); ?????
        ArrayList<LinearLayout> alSzenenLayouts = new ArrayList();
        int i;
        Cursor cSzenenPortrait;
        Cursor cSzenenLandscape;
        TextView TagTextViewSzenenLeft;
        TextView TagTextViewSzenenRight;
        TextView TagTextViewSzenenMiddle;
        Cursor cSzenen;
        if (nfctag.getRuleDaytime() == 1) {
            if (collectDaytimes().size() > 0) {
                it = collectDaytimes().iterator();
                while (it.hasNext()) {
                    Daytime dt = (Daytime) it.next();
                    if (App.isOrienationActivated()) {
                        for (i = 0; i <= 1; i++) {
                            cSzenenPortrait = App.getDB().rawQuery("SELECT actionID, scenename FROM actions WHERE FK_daytime=" + dt.getDaytimeId() + " AND FK_nfctag=" + nfctagId + " AND orientation=0", null);
                            cSzenenLandscape = App.getDB().rawQuery("SELECT actionID, scenename FROM actions WHERE FK_daytime=" + dt.getDaytimeId() + " AND FK_nfctag=" + nfctagId + " AND orientation=1", null);
                            LinearLayout _linearLayout = new LinearLayout(this);
                            _linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                            MarginLayoutParams _layoutParams = new LinearLayout.LayoutParams(-1, -2);
                            if (i == 1) {
                                _layoutParams.bottomMargin = 1;
                            }
                            _linearLayout.setLayoutParams(_layoutParams);
                            _linearLayout.setBackgroundResource(R.drawable.button);
                            LinearLayout _linearLayout1 = new LinearLayout(this);
                            _linearLayout1.setOrientation(LinearLayout.VERTICAL);
                            _linearLayout1.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
                            _linearLayout1.setPadding(5, 0, 5, 0);
                            LinearLayout _linearLayout2 = new LinearLayout(this);
                            _linearLayout2.setOrientation(LinearLayout.VERTICAL);
                            _linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(60, -2));
                            LinearLayout _linearLayout3 = new LinearLayout(this);
                            _linearLayout3.setOrientation(LinearLayout.VERTICAL);
                            _linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
                            _linearLayout3.setPadding(5, 0, 5, 0);
                            TagTextViewSzenenLeft = new TextView(this);
                            TagTextViewSzenenRight = new TextView(this);
                            TagTextViewSzenenMiddle = new TextView(this);
                            _linearLayout.addView(_linearLayout1);
                            _linearLayout.addView(_linearLayout2);
                            _linearLayout.addView(_linearLayout3);
                            _linearLayout1.addView(TagTextViewSzenenLeft);
                            _linearLayout2.addView(TagTextViewSzenenMiddle);
                            _linearLayout3.addView(TagTextViewSzenenRight);
                            TagTextViewSzenenRight.setTextSize(12.0f);
                            TagTextViewSzenenRight.setWidth(-2);
                            TagTextViewSzenenRight.setGravity(5);
                            TagTextViewSzenenRight.setSingleLine(true);
                            TagTextViewSzenenRight.setEllipsize(TruncateAt.END);
                            TagTextViewSzenenRight.setVisibility(View.VISIBLE);
                            TagTextViewSzenenLeft.setTextSize(12.0f);
                            TagTextViewSzenenLeft.setWidth(-2);
                            TagTextViewSzenenLeft.setSingleLine(true);
                            TagTextViewSzenenLeft.setEllipsize(TruncateAt.END);
                            TagTextViewSzenenLeft.setVisibility(View.VISIBLE);
                            TagTextViewSzenenMiddle.setTextSize(12.0f);
                            TagTextViewSzenenMiddle.setWidth(-2);
                            if (i == 0) {
                                TagTextViewSzenenMiddle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.portrait, 0, 0, 0);
                            }
                            if (i == 1) {
                                TagTextViewSzenenMiddle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.landscape, 0, 0, 0);
                            }
                            TagTextViewSzenenMiddle.setVisibility(View.VISIBLE);
                            if (i == 0) {
                                if (cSzenenPortrait.moveToLast()) {
                                    TagTextViewSzenenLeft.setText(dt.getName());
                                    TagTextViewSzenenMiddle.setText("\u2192");
                                    TagTextViewSzenenRight.setText(cSzenenPortrait.getString(1));
                                    _linearLayout.setId(cSzenenPortrait.getInt(0));
                                    alSzenenLayouts.add(_linearLayout);
                                } else {
                                    TagTextViewSzenenLeft.setText(dt.getName());
                                    TagTextViewSzenenMiddle.setText("\u2192");
                                    TagTextViewSzenenRight.setText(getString(R.string.text_nfctags_nicht_definiert));
                                }
                            }
                            if (i == 1) {
                                if (cSzenenLandscape.moveToLast()) {
                                    TagTextViewSzenenMiddle.setText("\u2192");
                                    TagTextViewSzenenRight.setText(cSzenenLandscape.getString(1));
                                    _linearLayout.setId(cSzenenLandscape.getInt(0));
                                    alSzenenLayouts.add(_linearLayout);
                                } else {
                                    TagTextViewSzenenMiddle.setText("\u2192");
                                    TagTextViewSzenenRight.setText(getString(R.string.text_nfctags_nicht_definiert));
                                }
                            }
                            linearLayout2.addView(_linearLayout);
                        }
                    } else {
                        cSzenen = App.getDB().rawQuery("SELECT actionID, scenename FROM actions WHERE FK_daytime=" + dt.getDaytimeId() + " AND FK_nfctag=" + nfctagId + " AND orientation=0", null);
                        LinearLayout _linearLayout = new LinearLayout(this);
                        _linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                        MarginLayoutParams _layoutParams = new LinearLayout.LayoutParams(-1, -2);
                        _layoutParams.bottomMargin = 1;
                        _linearLayout.setLayoutParams(_layoutParams);
                        _linearLayout.setBackgroundResource(R.drawable.button);
                        LinearLayout _linearLayout1 = new LinearLayout(this);
                        _linearLayout1.setOrientation(LinearLayout.VERTICAL);
                        _linearLayout1.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
                        _linearLayout1.setPadding(5, 0, 5, 0);
                        LinearLayout _linearLayout2 = new LinearLayout(this);
                        _linearLayout2.setOrientation(LinearLayout.VERTICAL);
                        _linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(40, -2));
                        LinearLayout _linearLayout3 = new LinearLayout(this);
                        _linearLayout3.setOrientation(LinearLayout.VERTICAL);
                        _linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
                        _linearLayout3.setPadding(5, 0, 5, 0);
                        TagTextViewSzenenLeft = new TextView(this);
                        TagTextViewSzenenRight = new TextView(this);
                        TagTextViewSzenenMiddle = new TextView(this);
                        _linearLayout.addView(_linearLayout1);
                        _linearLayout.addView(_linearLayout2);
                        _linearLayout.addView(_linearLayout3);
                        _linearLayout1.addView(TagTextViewSzenenLeft);
                        _linearLayout2.addView(TagTextViewSzenenMiddle);
                        _linearLayout3.addView(TagTextViewSzenenRight);
                        TagTextViewSzenenRight.setTextSize(12.0f);
                        TagTextViewSzenenRight.setWidth(-2);
                        TagTextViewSzenenRight.setGravity(5);
                        TagTextViewSzenenRight.setSingleLine(true);
                        TagTextViewSzenenRight.setEllipsize(TruncateAt.END);
                        TagTextViewSzenenRight.setVisibility(View.VISIBLE);
                        TagTextViewSzenenLeft.setTextSize(12.0f);
                        TagTextViewSzenenLeft.setWidth(-2);
                        TagTextViewSzenenLeft.setSingleLine(true);
                        TagTextViewSzenenLeft.setEllipsize(TruncateAt.END);
                        TagTextViewSzenenLeft.setVisibility(View.VISIBLE);
                        TagTextViewSzenenMiddle.setTextSize(12.0f);
                        TagTextViewSzenenMiddle.setWidth(-2);
                        TagTextViewSzenenMiddle.setVisibility(View.VISIBLE);
                        if (cSzenen.moveToLast()) {
                            TagTextViewSzenenLeft.setText(dt.getName());
                            TagTextViewSzenenMiddle.setText("\u2192");
                            TagTextViewSzenenRight.setText(cSzenen.getString(1));
                            _linearLayout.setId(cSzenen.getInt(0));
                            alSzenenLayouts.add(_linearLayout);
                        } else {
                            TagTextViewSzenenLeft.setText(dt.getName());
                            TagTextViewSzenenMiddle.setText("\u2192");
                            TagTextViewSzenenRight.setText(getString(R.string.text_nfctags_nicht_definiert));
                        }
                        linearLayout2.addView(_linearLayout);
                    }
                }
            } else {
                LinearLayout _linearLayout = new LinearLayout(this);
                _linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                MarginLayoutParams _layoutParams = new LinearLayout.LayoutParams(-1, -2);
                _layoutParams.bottomMargin = 1;
                _linearLayout.setLayoutParams(_layoutParams);
                _linearLayout.setBackgroundResource(R.drawable.button);
                LinearLayout _linearLayout2 = new LinearLayout(this);
                _linearLayout2.setOrientation(LinearLayout.VERTICAL);
                _linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
                _linearLayout2.setPadding(5, 0, 5, 0);
                TagTextViewSzenenLeft = new TextView(this);
                _linearLayout.addView(_linearLayout2);
                _linearLayout2.addView(TagTextViewSzenenLeft);
                TagTextViewSzenenLeft.setTextSize(12.0f);
                TagTextViewSzenenLeft.setWidth(-2);
                TagTextViewSzenenLeft.setVisibility(View.VISIBLE);
                linearLayout2.addView(_linearLayout);
                TagTextViewSzenenLeft.setText(getString(R.string.text_tageszeiten_keine_vorhanden));
            }
        } else if (App.isOrienationActivated()) {
            for (i = 0; i <= 1; i++) {
                cSzenenPortrait = App.getDB().rawQuery("SELECT actionID, scenename FROM actions WHERE FK_nfctag=" + nfctagId + " AND FK_daytime=1 AND orientation=0", null);
                cSzenenLandscape = App.getDB().rawQuery("SELECT actionID, scenename FROM actions WHERE FK_nfctag=" + nfctagId + " AND FK_daytime=1 AND orientation=1", null);
                LinearLayout _linearLayout = new LinearLayout(this);
                _linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                MarginLayoutParams _layoutParams = new LinearLayout.LayoutParams(-1, -2);
                if (i == 1) {
                    _layoutParams.bottomMargin = 1;
                }
                _linearLayout.setLayoutParams(_layoutParams);
                _linearLayout.setBackgroundResource(R.drawable.button);
                LinearLayout _linearLayout1 = new LinearLayout(this);
                _linearLayout1.setOrientation(LinearLayout.VERTICAL);
                _linearLayout1.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
                _linearLayout1.setPadding(5, 0, 5, 0);
                LinearLayout _linearLayout2 = new LinearLayout(this);
                _linearLayout2.setOrientation(LinearLayout.VERTICAL);
                _linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(60, -2));
                LinearLayout _linearLayout3 = new LinearLayout(this);
                _linearLayout3.setOrientation(LinearLayout.VERTICAL);
                _linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
                _linearLayout3.setPadding(5, 0, 5, 0);
                TagTextViewSzenenLeft = new TextView(this);
                TagTextViewSzenenRight = new TextView(this);
                TagTextViewSzenenMiddle = new TextView(this);
                _linearLayout.addView(_linearLayout1);
                _linearLayout.addView(_linearLayout2);
                _linearLayout.addView(_linearLayout3);
                _linearLayout1.addView(TagTextViewSzenenLeft);
                _linearLayout2.addView(TagTextViewSzenenMiddle);
                _linearLayout3.addView(TagTextViewSzenenRight);
                TagTextViewSzenenRight.setTextSize(12.0f);
                TagTextViewSzenenRight.setWidth(-2);
                TagTextViewSzenenRight.setGravity(5);
                TagTextViewSzenenRight.setVisibility(View.VISIBLE);
                TagTextViewSzenenLeft.setTextSize(12.0f);
                TagTextViewSzenenLeft.setWidth(-2);
                TagTextViewSzenenLeft.setVisibility(View.VISIBLE);
                TagTextViewSzenenMiddle.setTextSize(12.0f);
                TagTextViewSzenenMiddle.setWidth(-2);
                if (i == 0) {
                    TagTextViewSzenenMiddle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.portrait, 0, 0, 0);
                }
                if (i == 1) {
                    TagTextViewSzenenMiddle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.landscape, 0, 0, 0);
                }
                TagTextViewSzenenMiddle.setVisibility(View.VISIBLE);
                if (i == 0) {
                    if (cSzenenPortrait.moveToLast()) {
                        TagTextViewSzenenLeft.setText(getString(R.string.text_nfctags_standardszene));
                        TagTextViewSzenenMiddle.setText("\u2192");
                        TagTextViewSzenenRight.setText(cSzenenPortrait.getString(1));
                        _linearLayout.setId(cSzenenPortrait.getInt(0));
                        alSzenenLayouts.add(_linearLayout);
                    } else {
                        TagTextViewSzenenLeft.setText(getString(R.string.text_nfctags_standardszene));
                        TagTextViewSzenenMiddle.setText("\u2192");
                        TagTextViewSzenenRight.setText(getString(R.string.text_nfctags_nicht_definiert));
                    }
                }
                if (i == 1) {
                    if (cSzenenLandscape.moveToLast()) {
                        TagTextViewSzenenMiddle.setText("\u2192");
                        TagTextViewSzenenRight.setText(cSzenenLandscape.getString(1));
                        _linearLayout.setId(cSzenenLandscape.getInt(0));
                        alSzenenLayouts.add(_linearLayout);
                    } else {
                        TagTextViewSzenenMiddle.setText("\u2192");
                        TagTextViewSzenenRight.setText(getString(R.string.text_nfctags_nicht_definiert));
                    }
                }
                linearLayout2.addView(_linearLayout);
            }
        } else {
            cSzenen = App.getDB().rawQuery("SELECT actionID, scenename FROM actions WHERE FK_nfctag=" + nfctagId + " AND FK_daytime=1 AND orientation=0", null);
            LinearLayout _linearLayout = new LinearLayout(this);
            _linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            MarginLayoutParams _layoutParams = new LinearLayout.LayoutParams(-1, -2);
            _layoutParams.bottomMargin = 1;
            _linearLayout.setLayoutParams(_layoutParams);
            _linearLayout.setBackgroundResource(R.drawable.button);
            LinearLayout _linearLayout1 = new LinearLayout(this);
            _linearLayout1.setOrientation(LinearLayout.VERTICAL);
            _linearLayout1.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
            _linearLayout1.setPadding(5, 0, 5, 0);
            LinearLayout _linearLayout2 = new LinearLayout(this);
            _linearLayout2.setOrientation(LinearLayout.VERTICAL);
            _linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(40, -2));
            LinearLayout _linearLayout3 = new LinearLayout(this);
            _linearLayout3.setOrientation(LinearLayout.VERTICAL);
            _linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
            _linearLayout3.setPadding(5, 0, 5, 0);
            TagTextViewSzenenLeft = new TextView(this);
            TagTextViewSzenenRight = new TextView(this);
            TagTextViewSzenenMiddle = new TextView(this);
            _linearLayout.addView(_linearLayout1);
            _linearLayout.addView(_linearLayout2);
            _linearLayout.addView(_linearLayout3);
            _linearLayout1.addView(TagTextViewSzenenLeft);
            _linearLayout2.addView(TagTextViewSzenenMiddle);
            _linearLayout3.addView(TagTextViewSzenenRight);
            TagTextViewSzenenRight.setTextSize(12.0f);
            TagTextViewSzenenRight.setWidth(-2);
            TagTextViewSzenenRight.setGravity(5);
            TagTextViewSzenenRight.setVisibility(View.VISIBLE);
            TagTextViewSzenenLeft.setTextSize(12.0f);
            TagTextViewSzenenLeft.setWidth(-2);
            TagTextViewSzenenLeft.setVisibility(View.VISIBLE);
            TagTextViewSzenenMiddle.setTextSize(12.0f);
            TagTextViewSzenenMiddle.setWidth(-2);
            TagTextViewSzenenMiddle.setVisibility(View.VISIBLE);
            if (cSzenen.moveToLast()) {
                TagTextViewSzenenLeft.setText(getString(R.string.text_nfctags_standardszene));
                TagTextViewSzenenMiddle.setText("\u2192");
                TagTextViewSzenenRight.setText(cSzenen.getString(1));
                _linearLayout.setId(cSzenen.getInt(0));
                alSzenenLayouts.add(_linearLayout);
            } else {
                TagTextViewSzenenLeft.setText(getString(R.string.text_nfctags_standardszene));
                TagTextViewSzenenMiddle.setText("\u2192");
                TagTextViewSzenenRight.setText(getString(R.string.text_nfctags_nicht_definiert));
            }
            linearLayout2.addView(_linearLayout);
        }
        if (isActiveNfctag(nfctag)) {
            layoutParams2.setMargins(changeDpToPx(0), changeDpToPx(0), changeDpToPx(0), changeDpToPx(0));
            linearLayout2.setLayoutParams(layoutParams2);
            linearLayout.addView(linearLayout2);
            imageView.setImageResource(R.drawable.tag32y);
            if (!CheckOps.getActionIdToCall(nfctag, getScreenOrientation(), this).equals(getString(R.string.notification_toast_keine_passende_szene))) {
                it = alSzenenLayouts.iterator();
                while (it.hasNext()) {
                    LinearLayout szenenLayout = (LinearLayout) it.next();
                    if (Integer.parseInt(CheckOps.getActionIdToCall(nfctag, getScreenOrientation(), this)) == szenenLayout.getId()) {
                        szenenLayout.setBackgroundColor(Color.rgb(MotionEventCompat.ACTION_MASK, 187, 51));
                    }
                }
            }
            linearLayout.setOnLongClickListener(new C00363(nfctag, linearLayout));
            return linearLayout;
        }
        linearLayout2.setOnLongClickListener(new C00374(nfctag, linearLayout2));
        return linearLayout2;
    }

    public boolean isActiveNfctag(NfcTag nfctag) {
        if (App.getLastScannedTag() == null || App.getLastScannedTag().getNfctagId() != nfctag.getNfctagId()) {
            return false;
        }
        return true;
    }

    public void showTagOptionsDialog(NfcTag nfctag, LinearLayout tagLayout) {
        Builder optionsDialog;
        if (nfctag.getRuleDaytime() == 1) {
            if (App.isOrienationActivated()) {
                optionsDialog = new Builder(this);
                optionsDialog.setItems(new String[]{getString(R.string.menu_text_nfctags_loeschen), getString(R.string.menu_text_nfctags_umbenennen), getString(R.string.menu_text_nfctags_szene_zuweisen_portrait), getString(R.string.menu_text_nfctags_szene_zuweisen_landscape), getString(R.string.menu_text_nfctags_alle_szenen_trennen), getString(R.string.menu_text_nfctags_tageszeit_deaktivieren)}, new C00385(nfctag, tagLayout));
                optionsDialog.show();
                return;
            }
            optionsDialog = new Builder(this);
            optionsDialog.setItems(new String[]{getString(R.string.menu_text_nfctags_loeschen), getString(R.string.menu_text_nfctags_umbenennen), getString(R.string.menu_text_nfctags_szene_zuweisen), getString(R.string.menu_text_nfctags_alle_szenen_trennen), getString(R.string.menu_text_nfctags_tageszeit_deaktivieren)}, new C00396(nfctag, tagLayout));
            optionsDialog.show();
        } else if (App.isOrienationActivated()) {
            optionsDialog = new Builder(this);
            optionsDialog.setItems(new String[]{getString(R.string.menu_text_nfctags_loeschen), getString(R.string.menu_text_nfctags_umbenennen), getString(R.string.menu_text_nfctags_szene_zuweisen_portrait), getString(R.string.menu_text_nfctags_szene_zuweisen_landscape), getString(R.string.menu_text_nfctags_alle_szenen_trennen), getString(R.string.menu_text_nfctags_tageszeit_aktivieren)}, new C00407(nfctag, tagLayout));
            optionsDialog.show();
        } else {
            optionsDialog = new Builder(this);
            optionsDialog.setItems(new String[]{getString(R.string.menu_text_nfctags_loeschen), getString(R.string.menu_text_nfctags_umbenennen), getString(R.string.menu_text_nfctags_szene_zuweisen), getString(R.string.menu_text_nfctags_alle_szenen_trennen), getString(R.string.menu_text_nfctags_tageszeit_aktivieren)}, new C00418(nfctag, tagLayout));
            optionsDialog.show();
        }
    }

    public void reloadNfctags() {
        LinearLayout llNfctags = (LinearLayout) findViewById(R.id.linearlayout_nfctags);
        llNfctags.removeAllViews();
        if (collectNfcTags().size() > 0) {
            Iterator it = collectNfcTags().iterator();
            while (it.hasNext()) {
                llNfctags.addView(createTagLayout((NfcTag) it.next()));
            }
            return;
        }
        TextView tv = new TextView(this);
        tv.setText(getString(R.string.start_text_nfctags_erfassen));
        tv.setWidth(-1);
        tv.setGravity(17);
        tv.setTextSize(14.0f);
        tv.setPadding(20, 10, 10, 20);
        tv.setBackgroundColor(-1);
        llNfctags.addView(tv);
    }

    public void reloadNfctags(View view) {
        reloadNfctags();
    }

    public void highlightViewWithAnimation(LinearLayout nfctagTotalLayout) {
        Animation remove = AnimationUtils.loadAnimation(this, R.anim.highlight1);
        Animation add = AnimationUtils.loadAnimation(this, R.anim.highlight2);
        nfctagTotalLayout.startAnimation(remove);
        remove.setAnimationListener(new C00429(nfctagTotalLayout, add));
    }

    public void highlightViewWithAnimation2(LinearLayout linearLayout) {
        Animation remove = AnimationUtils.loadAnimation(this, R.anim.highlight3);
        Animation add = AnimationUtils.loadAnimation(this, R.anim.highlight4);
        linearLayout.startAnimation(remove);
        remove.setAnimationListener(new AnonymousClass10(linearLayout, add));
    }

    public void removeViewWithAnimation(LinearLayout nfctagTotalLayout) {
        Animation remove = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        nfctagTotalLayout.startAnimation(remove);
        remove.setAnimationListener(new AnonymousClass11(nfctagTotalLayout));
    }

    public void showNfctagRenameDialog(NfcTag nfctag, LinearLayout nfcTagLayout) {
        Builder nfctagNameInputDialog = new Builder(this);
        nfctagNameInputDialog.setTitle(getString(R.string.dialog_nfctags_umbenennen));
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        TextView title = new TextView(this);
        title.setText(getString(R.string.dialog_nfctags_umbenennen_name));
        title.setTextColor(Color.LTGRAY);
        title.setPadding(10, 0, 0, 0);
        TextView title2 = new TextView(this);
        title2.setText(getString(R.string.dialog_nfctags_umbenennen_hinweis));
        title2.setTextColor(Color.LTGRAY);
        title2.setPadding(10, 0, 0, 0);
        EditText input = new EditText(this);
        input.setText(nfctag.getNfctagName());
        EditText input2 = new EditText(this);
        input2.setText(nfctag.getNfctagHint());
        ll.addView(title);
        ll.addView(input);
        ll.addView(title2);
        ll.addView(input2);
        nfctagNameInputDialog.setView(ll);
        nfctagNameInputDialog.setPositiveButton(getString(R.string.button_text_speichern), new AnonymousClass12(input, input2, nfctag, nfcTagLayout));
        nfctagNameInputDialog.setNegativeButton(getString(R.string.button_text_speichern), new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        nfctagNameInputDialog.show();
    }

    public void showNfctagLinkerDialog(NfcTag nfctag, LinearLayout tagLayout) {
        Builder nfctagLinkerInputDialog = new Builder(this);
        nfctagLinkerInputDialog.setTitle(getString(R.string.dialog_nfctags_tageszeit_waehlen));
        RadioGroup radiogroup = new RadioGroup(this);
        radiogroup.setPadding(10, 10, 10, 10);
        Iterator it = collectDaytimes().iterator();
        while (it.hasNext()) {
            Daytime daytime = (Daytime) it.next();
            RadioButton daytimeRadioButton = new RadioButton(this);
            daytimeRadioButton.setText(daytime.getName());
            daytimeRadioButton.setTextSize(14.0f);
            daytimeRadioButton.setWidth(-1);
            daytimeRadioButton.setVisibility(View.VISIBLE);
            daytimeRadioButton.setId(daytime.getDaytimeId());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, -1);
            params.bottomMargin = 1;
            daytimeRadioButton.setLayoutParams(params);
            radiogroup.addView(daytimeRadioButton);
        }
        nfctagLinkerInputDialog.setView(radiogroup);
        nfctagLinkerInputDialog.setPositiveButton(getString(R.string.button_text_weiter), new AnonymousClass14(radiogroup, nfctag, tagLayout));
        nfctagLinkerInputDialog.setNegativeButton(getString(R.string.button_text_speichern), new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        nfctagLinkerInputDialog.show();
    }
    
    public void showSceneConfirmationDialogWithActiveDaytime(String scene, LinearLayout tagLayout, NfcTag nfctag) {
        Builder sceneConfDialog = new Builder(this);
        Cursor cChosenDaytime = App.getDB().rawQuery("SELECT daytimename FROM daytime WHERE daytimeID=" + this.actualDaytimeId, null);
        cChosenDaytime.moveToFirst();
        sceneConfDialog.setMessage(getString(R.string.szene_uebernehmen_text) + ". \r\n" + getString(R.string.szene_uebernehmen_name) + ": " + scene + "\r\n" + getString(R.string.szene_uebernehmen_tageszeit) + ": " + cChosenDaytime.getString(0) + "\r\n" + getString(R.string.szene_uebernehmen_nfctag) + ": " + this.actualNfcTag.getNfctagName());
        sceneConfDialog.setPositiveButton(getString(R.string.button_text_ok), new AnonymousClass16(tagLayout, nfctag));
        sceneConfDialog.setCancelable(false);
        sceneConfDialog.show();
    }

    public void showSceneConfirmationDialogWithoutActiveDaytime(String scene, LinearLayout tagLayout, NfcTag nfctag) {
        Builder sceneConfDialog = new Builder(this);
        sceneConfDialog.setMessage(getString(R.string.szene_uebernehmen_text) + ". \r\n" + getString(R.string.szene_uebernehmen_name) + ": " + scene + "\r\n" + getString(R.string.szene_uebernehmen_nfctag) + ": " + this.actualNfcTag.getNfctagName());
        sceneConfDialog.setPositiveButton(getString(R.string.button_text_ok), new AnonymousClass17(tagLayout, nfctag));
        sceneConfDialog.setCancelable(false);
        sceneConfDialog.show();
    }

    public void showNfctagInfo(View view) {
        Builder nfctagInfoDialog = new Builder(this);
        LinearLayout infoLayout = new LinearLayout(this);
        infoLayout.setOrientation(LinearLayout.VERTICAL);
        infoLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        TextView tvTitel1 = new TextView(this);
        tvTitel1.setText(getString(R.string.info_text_title1));
        tvTitel1.setTypeface(null, 1);
        tvTitel1.setTextSize(14.0f);
        tvTitel1.setPadding(20, 20, 20, 5);
        tvTitel1.setWidth(-2);
        tvTitel1.setVisibility(View.VISIBLE);
        TextView tvText1 = new TextView(this);
        tvText1.setText(getString(R.string.info_text_text1));
        tvText1.setTextSize(14.0f);
        tvText1.setPadding(20, 0, 20, 20);
        tvText1.setWidth(-2);
        tvText1.setVisibility(View.VISIBLE);
        TextView tvTitel2 = new TextView(this);
        tvTitel2.setText(getString(R.string.info_text_title2));
        tvTitel2.setTypeface(null, 1);
        tvTitel2.setTextSize(14.0f);
        tvTitel2.setPadding(20, 0, 20, 5);
        tvTitel2.setWidth(-2);
        tvTitel2.setVisibility(View.VISIBLE);
        TextView tvText2 = new TextView(this);
        tvText2.setText(getString(R.string.info_text_text2));
        tvText2.setTextSize(14.0f);
        tvText2.setPadding(20, 0, 20, 20);
        tvText2.setWidth(-2);
        tvText2.setVisibility(View.VISIBLE);
        TextView tvTitel3 = new TextView(this);
        tvTitel3.setText(getString(R.string.info_text_title3));
        tvTitel3.setTypeface(null, 1);
        tvTitel3.setTextSize(14.0f);
        tvTitel3.setPadding(20, 0, 20, 5);
        tvTitel3.setWidth(-2);
        tvTitel3.setVisibility(View.VISIBLE);
        TextView tvText3 = new TextView(this);
        tvText3.setText(getString(R.string.info_text_text3));
        tvText3.setTextSize(14.0f);
        tvText3.setPadding(20, 0, 20, 20);
        tvText3.setWidth(-2);
        tvText3.setVisibility(View.VISIBLE);
        ScrollView scroll = new ScrollView(this);
        infoLayout.addView(tvTitel1);
        infoLayout.addView(tvText1);
        infoLayout.addView(tvTitel2);
        infoLayout.addView(tvText2);
        infoLayout.addView(tvTitel3);
        infoLayout.addView(tvText3);
        scroll.addView(infoLayout);
        nfctagInfoDialog.setView(scroll);
        nfctagInfoDialog.setPositiveButton(getString(R.string.button_text_ok), new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        nfctagInfoDialog.show();
    }

    public boolean getSystemSettingRotation() {
        if (System.getInt(getContentResolver(), "accelerometer_rotation", 0) == 1) {
            return true;
        }
        return false;
    }

    public int getScreenOrientation() {
        int orientation = getWindowManager().getDefaultDisplay().getRotation();
        if (orientation != 0) {
            return 1;
        }
        return orientation;
    }

    public NdefMessage createMessage() {
        NdefRecord mimeRecord = NdefRecord.createMime("application/ch.zingge.ds_nfc", "dS NFC".getBytes(Charset.forName("US-ASCII")));
        return new NdefMessage(new NdefRecord[]{mimeRecord});
    }

    public void writeOnTag(NdefMessage message, Tag tag) {
        try {
            Ndef ndef = Ndef.get(tag);
            if (ndef != null) {
                ndef.connect();
                if (ndef.isWritable()) {
                    ndef.writeNdefMessage(message);
                    Toast.makeText(this, getString(R.string.dstag_toast_erfolgreich), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, getString(R.string.dstag_toast_schreibschutz), Toast.LENGTH_LONG).show();
                }
                this.dsTagCreatable = false;
            }
            Toast.makeText(this, getString(R.string.dstag_toast_nondef), Toast.LENGTH_LONG).show();
            this.dsTagCreatable = false;
        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.dstag_toast_other_exception) + " (" + e.toString() + ")", Toast.LENGTH_LONG).show();
        }
    }

    public void showWriteTagDialog(View view) {
        Builder writeNfctagDialog = new Builder(this);
        LinearLayout infoLayout = new LinearLayout(this);
        infoLayout.setOrientation(LinearLayout.VERTICAL);
        infoLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        TextView tvTitel = new TextView(this);
        tvTitel.setText(getString(R.string.dstag_erstellen_title));
        tvTitel.setTypeface(null, 1);
        tvTitel.setTextSize(14.0f);
        tvTitel.setPadding(20, 20, 20, 5);
        tvTitel.setWidth(-2);
        tvTitel.setVisibility(View.VISIBLE);
        TextView tvText = new TextView(this);
        tvText.setText(getString(R.string.dstag_erstellen_text));
        tvText.setTextSize(14.0f);
        tvText.setPadding(20, 0, 20, 20);
        tvText.setWidth(-2);
        tvText.setVisibility(View.VISIBLE);
        infoLayout.addView(tvTitel);
        infoLayout.addView(tvText);
        writeNfctagDialog.setView(infoLayout);
        writeNfctagDialog.setNegativeButton(getString(R.string.button_text_speichern), new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        writeNfctagDialog.setPositiveButton(getString(R.string.button_text_ok), new OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MainActivity.this.dsTagCreatable = true;
                LinearLayout barTitelOptions = (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_titel_all_main);
                MainActivity.this.changeLayout(barTitelOptions, (LinearLayout) MainActivity.this.findViewById(R.id.linearlayout_yellow_all_main));
            }
        });
        writeNfctagDialog.show();
    }

    private PendingIntent getPendingIntent() {
        return PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(536870912), 0);
    }

    private void enableNfcWrite() {
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter != null) {
            nfcAdapter.enableForegroundDispatch(this, getPendingIntent(), new IntentFilter[]{new IntentFilter("android.nfc.action.TAG_DISCOVERED")}, null);
        }
    }

    public void onNewIntent(Intent intent) {
        // System.out.println(intent.getAction());
        if (this.dsTagCreatable) {
            writeOnTag(createMessage(), (Tag) intent.getParcelableExtra("android.nfc.extra.TAG"));
        } else if (!this.dsTagCreatable && !intent.getAction().equals("android.intent.action.MAIN")) {
            Tag detectedTag = (Tag) intent.getParcelableExtra("android.nfc.extra.TAG");
            this.actualNfcHexId = changeIdtoHexString(detectedTag.getId());
            if (checkNfcTagExisting(changeIdtoHexString(detectedTag.getId()))) {
                Cursor cFoundTag = App.getDB().rawQuery("SELECT * FROM nfctag WHERE nfctag_hexID='" + this.actualNfcHexId + "'", null);
                cFoundTag.moveToFirst();
                NfcTag lastScannedTag = new NfcTag();
                lastScannedTag.setNfctagId(cFoundTag.getInt(0));
                lastScannedTag.setNfctagHexId(cFoundTag.getString(1));
                lastScannedTag.setNfctagName(cFoundTag.getString(2));
                lastScannedTag.setNfctagHint(cFoundTag.getString(3));
                lastScannedTag.setRuleDaytime(cFoundTag.getInt(4));
                App.setLastScannedTag(lastScannedTag);
                if (CheckOps.getActionIdToCall(lastScannedTag, getScreenOrientation(), this).length() <= 4) {
                    App.getDB().rawQuery("SELECT scenename FROM actions WHERE actionID=" + CheckOps.getActionIdToCall(lastScannedTag, getScreenOrientation(), this), null).moveToFirst();
                    callIntent(CheckOps.getActionIdToCall(lastScannedTag, getScreenOrientation(), this));
                } else {
                    Toast.makeText(this, CheckOps.getActionIdToCall(lastScannedTag, getScreenOrientation(), this), Toast.LENGTH_LONG).show();
                }
                reloadNfctags();
                return;
            }
            showNfcTagInputDialog(changeIdtoHexString(detectedTag.getId()));
        }
    }

    public void onPause() {
        super.onPause();
        LinearLayout barYellow = (LinearLayout) findViewById(R.id.linearlayout_yellow_all_main);
        LinearLayout barTitelOptions = (LinearLayout) findViewById(R.id.linearlayout_titel_all_main);
        if (this.dsTagCreatable) {
            changeLayout(barYellow, barTitelOptions);
        }
    }

    public void onResume() {
        super.onResume();
        enableNfcWrite();
        if (!this.comeFromdSApp) {
            reloadNfctags();
        }
        this.comeFromdSApp = false;
    }

    public void cancelWriteTag(View view) {
        this.dsTagCreatable = false;
        changeLayout((LinearLayout) findViewById(R.id.linearlayout_yellow_all_main), (LinearLayout) findViewById(R.id.linearlayout_titel_all_main));
    }

    public void changeLayout(LinearLayout llOld, LinearLayout llNew) {
        Animation out = AnimationUtils.loadAnimation(this, R.anim.fadeout_short);
        llOld.startAnimation(out);
        out.setAnimationListener(new AnonymousClass21(llOld, llNew));
    }
}
